package by.epam.lab.newsmanagment.service.impl;

import by.epam.lab.newsmanagment.dao.INewsDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.*;
import by.epam.lab.newsmanagment.domain.dto.NewsTO;
import by.epam.lab.newsmanagment.service.IAuthorService;
import by.epam.lab.newsmanagment.service.ICommentService;
import by.epam.lab.newsmanagment.service.INewsService;
import by.epam.lab.newsmanagment.service.ITagService;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.service.INewsService
 */
@Service("newsService")
public class NewsService implements INewsService {

    private static final int NEWS_PER_PAGE = 3;

    @Autowired
    private INewsDAO newsDAO;

    @Autowired
    private IAuthorService authorService;

    @Autowired
    private ITagService tagService;

    @Autowired
    private ICommentService commentService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long add(NewsRecord newsRecord) throws IServiceException {
        Long newsId;
        News news = newsRecord.getNews();
        Long authorId = newsRecord.getAuthorId();
        List<Long> tagIdList = newsRecord.getTagIdList();

        try {
            newsId = newsDAO.create(news);
            newsDAO.connectWithAuthor(newsId, authorId);
            if(tagIdList != null){
                for(Long tagId : tagIdList){
                    newsDAO.connectWithTag(newsId, tagId);
                }
            }
        } catch (IDAOException e) {
            throw new IServiceException("Exception in NewsService", e);
        }
        return newsId;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(NewsRecord newsRecord) throws IServiceException {
        News news = newsRecord.getNews();
        Long authorId = newsRecord.getAuthorId();
        List<Long> tagIdList = newsRecord.getTagIdList();
        try {
            newsDAO.update(news.getId(), news);
            newsDAO.disconnectWithAuthor(news.getId());
            newsDAO.connectWithAuthor(news.getId(), authorId);
            newsDAO.disconnectWithTags(news.getId());
            for(Long tagId : tagIdList){
                newsDAO.connectWithTag(news.getId(), tagId);
            }
        } catch (IDAOException e) {
            throw new IServiceException("Exception in NewsService", e);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Long newsId) throws IServiceException {

        try {
            News news = newsDAO.read(newsId);
            Author author = authorService.getByNewsId(newsId);
            List<Comment> commentList = commentService.getAllByNewsId(newsId);
            List<Tag> tagList = tagService.getAllByNewsId(newsId);

            newsDAO.disconnectWithAuthor(newsId);
            authorService.makeExpired(author);
            for(Comment com : commentList){
                commentService.delete(com.getComId());
            }
            for(Tag tag : tagList){
                newsDAO.disconnectWithTag(news.getId(), tag.getId());
            }
            newsDAO.delete(news.getId());

        } catch (IDAOException e) {
            throw new IServiceException("Exception in NewsService", e);
        }
    }

    @Override
    public NewsTO get(Long newsId) throws IServiceException {
        NewsTO newsTO = null;
        try {
            News news = newsDAO.read(newsId);
            Author author = authorService.getByNewsId(newsId);
            List<Tag> tagList = tagService.getAllByNewsId(newsId);
            List<Comment> commentList = commentService.getAllByNewsId(newsId);
            newsTO = new NewsTO();
            newsTO.setNews(news);
            newsTO.setAuthor(author);
            newsTO.setTagList(tagList);
            newsTO.setCommentList(commentList);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in NewsService", e);
        }
        return newsTO;
    }

    @Override
    public int countPages(SearchCriteria searchCriteria) throws IServiceException {
        int pages = 0;
        try {
            Set<Long> authorsId = searchCriteria.getAuthorIdSet();
            Set<Long> tagIdSet = searchCriteria.getTagIdSet();
            int newsAmount = newsDAO.countNews(tagIdSet, authorsId);
            pages = (newsAmount % NEWS_PER_PAGE > 0) ? newsAmount / NEWS_PER_PAGE + 1 : newsAmount / NEWS_PER_PAGE ;
        } catch (IDAOException e) {
            throw new IServiceException("Exception in NewsService", e);
        }
        return pages;
    }

    @Override
    public List<NewsTO> getNews(SearchCriteria searchCriteria, int page) throws IServiceException {
        List<NewsTO> newsTOList = null;
        try {
            Set<Long> tagIdSet = searchCriteria.getTagIdSet() ;
            Set<Long> authorIdSet = searchCriteria.getAuthorIdSet() ;
            List<Long> newsIdSet = newsDAO.readAll(tagIdSet, authorIdSet, NEWS_PER_PAGE, page);
            newsTOList = createNewsTOSet(newsIdSet);
        } catch (IDAOException e){
            throw new IServiceException("Exception in NewsService", e);
        }
        return newsTOList;
    }

    /**
     * Form a newsTO set from set of news id.
     * @param newsIdList
     * @return set of newsTO
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    private List<NewsTO> createNewsTOSet(List<Long> newsIdList) throws IDAOException, IServiceException {

        List<NewsTO> newsTOList = new ArrayList<>();
        NewsTO newsTO = null;
        for(Long newsId : newsIdList){
            newsTO = get(newsId);
            newsTOList.add(newsTO);
        }
        return newsTOList;
    }
}
