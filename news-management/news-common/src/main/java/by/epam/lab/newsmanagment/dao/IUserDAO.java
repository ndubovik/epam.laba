package by.epam.lab.newsmanagment.dao;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.User;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.GenericDAO
 */
public interface IUserDAO extends GenericDAO<Long, User> {

    /**
     * Find id of user by it's login
     * @param login
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return user id
     */
    Long findId(String login) throws IDAOException;

}
