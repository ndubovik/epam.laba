package by.epam.lab.newsmanagment.domain;

import java.io.Serializable;

/**
 * @author Nastya Dubovik
 */
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private String login;
	private String password;
	
	public User(Long id, String name, String login, String password){
		this.id = id;
		this.name = name;
		this.login = login;
		this.password = password;
	}

	public User(String name, String login, String password){
		this.id = null;
		this.name = name;
		this.login = login;
		this.password = password;
	}

	public User() {
		this.id = null;
		this.name = new String();
		this.login = new String();
		this.password = new String();
	}

	public Long getId(){
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getLogin(){
		return login;
	}
	
	public void setLogin(String login){
		this.login = login;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}

	/**
	 * Compare this object with object that are sent
	 * @param obj
	 * @return true if this object equals to obj, false if this object doesn't equal to obj
	 */
	@Override
	public boolean equals(Object obj){
		if(this == obj){
			return true;
		}
		if(obj == null){
			return false;
		}
		if(this.getClass() != obj.getClass()){
			return false;
		}
		User o = (User)obj;
		if(! this.name.equals(o.name)){
			return false;
		}
		if(! this.login.equals(o.login)){
			return false;
		}
		if(! this.password.equals(o.password)){
			return false;
		}
		return true;
	}

	/**
	 * Create a string that contains all information about the object
	 * @return string with full information
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getName());
		sb.append(": id = " + id);
		sb.append(", name = " + name);
		sb.append(", login = " + login);
		sb.append(", password = " + password);
		return sb.toString();
	}

	/**
	 * Depending on main fields creat a hash code
	 * @return hash code
	 */
	@Override
	public int hashCode(){
		int hash = name.hashCode()*31 + login.hashCode()*31 + password.hashCode()*31 ;
		return hash;
	}
}
