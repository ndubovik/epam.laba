package by.epam.lab.newsmanagment.dao;

import java.util.List;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;


/**
 * @author Nastya Dubovik
 */
public interface  GenericDAO<K,T> {

	/**
	 * Create row in table with required parameters.
	 * @param entity
	 * @return the id of row in table, null will never be returned
	 * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
	K create(T entity) throws IDAOException;

	/**
	 * Read the row from table by key.
	 * @param key
	 * @return the entity that has been found
	 * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
	T read(K key) throws IDAOException;

	/**
	 * Read all rows from table.
	 * @return the list of entities
	 * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
	List<T> readAll() throws IDAOException;

	/**
	 * Update the row in table by key, replace values of column by the same values from entity.
	 * @param key
	 * @param entity
	 * @return true if the operation has been successfully completed, in other way - false
	 * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
	void update(K key, T entity) throws IDAOException;

	/**
	 * Delete the row from table by key.
	 * @param key
	 * @return true if the operation has been successfully completed, in other way - false
	 * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
	void delete(K key) throws IDAOException;

}
