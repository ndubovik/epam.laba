package by.epam.lab.newsmanagment.service.impl;

import by.epam.lab.newsmanagment.dao.ITagDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Tag;
import by.epam.lab.newsmanagment.service.ITagService;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.service.ITagService
 */
@Service("tagService")
public class TagService implements ITagService {

    @Autowired
    private ITagDAO tagDAO;

    @Override
    public Long add (Tag tag) throws IServiceException {
        Long id = null;
        try {
            List<Tag> tagList = tagDAO.readAll();
            if(! tagList.contains(tag) ) {
                id = tagDAO.create(tag);
            }
        } catch (IDAOException e) {
            throw new IServiceException("Exception in TagService", e);
        }
        return id;
    }


    @Override
    public void delete(Long tagId) throws IServiceException {
        try {
            tagDAO.disconnectWithNews(tagId);
            tagDAO.delete(tagId);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in TagService", e);
        }
    }


    @Override
    public List<Tag> getAll() throws IServiceException {
        List<Tag> tagList = null;
        try {
            tagList = tagDAO.readAll();
        } catch (IDAOException e) {
            throw new IServiceException("Exception in TagService", e);
        }
        return tagList;
    }

    @Override
    public List<Tag> getAllByNewsId(Long newsId) throws IServiceException {
        List<Tag> tagList = null;
        try {
            List<Long> listOfTagId = tagDAO.findIdByNewsId(newsId);
            tagList = new ArrayList<>();
            for(Long id : listOfTagId){
                tagList.add(tagDAO.read(id));
            }
        } catch (IDAOException e) {
            throw new IServiceException("Exception in TagService", e);
        }
        return tagList;
    }

    @Override
    public Tag getByName(String name) throws IServiceException {
        Tag tag = null;
        try {
            tag = tagDAO.findByName(name);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in TagService", e);
        }
        return tag;
    }

    @Override
    public void edit (Tag tag) throws IServiceException {

        try {
            tagDAO.update(tag.getId(), tag);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Tag Service", e);
        }
    }

    @Override
    public Tag getById (Long tagId) throws IServiceException {
        Tag tag = null;
        try {
            tag = tagDAO.read(tagId);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Tag Service", e);
        }
        return tag;
    }

}
