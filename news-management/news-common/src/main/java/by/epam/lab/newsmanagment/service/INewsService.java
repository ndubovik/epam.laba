package by.epam.lab.newsmanagment.service;

import by.epam.lab.newsmanagment.domain.NewsRecord;
import by.epam.lab.newsmanagment.domain.SearchCriteria;
import by.epam.lab.newsmanagment.domain.dto.NewsTO;
import by.epam.lab.newsmanagment.exception.service.IServiceException;

import java.util.List;

/**
 * @author Nastya Dubovik
 */
public interface INewsService {

    /**
     * Add news and all data that is connected
     * with news (author, tag ...) to database.
     * @param newsRecord
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return id of news that has been created
     */
    Long add (NewsRecord newsRecord) throws IServiceException;

    /**
     * Edit news and data that is connected
     * with that news.
     * @param newsRecord
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return role id of required user id
     */
    void edit (NewsRecord newsRecord) throws IServiceException;

    /**
     * Delete news and data that is connected
     * with that news.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return role id of required user id
     */
    void delete (Long newsId) throws IServiceException;

    /**
     * Get news and all data that is connected
     * with news by news id.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return role id of required user id
     */
    NewsTO get (Long newsId) throws IServiceException;

    /**
     * Get all news that are sorted by search criteria.
     * @param searchCriteria
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return set of newsDTO
     */
    List<NewsTO> getNews (SearchCriteria searchCriteria, int page) throws IServiceException;

    /**
     * At first count news that are meet with search criteria. And divide it in pages.
     * @param searchCriteria
     * @return
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    int countPages(SearchCriteria searchCriteria) throws IServiceException;
}
