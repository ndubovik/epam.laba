package by.epam.lab.newsmanagment.dao.impl.oracle;

import by.epam.lab.newsmanagment.dao.ITagDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.exception.dao.impl.TagDAOException;
import by.epam.lab.newsmanagment.domain.Tag;
import by.epam.lab.newsmanagment.utils.MakeStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.ITagDAO
 */
@Component
public class TagDAO implements ITagDAO {

    private static final String SQL_CREATE_TAG = "INSERT INTO TAGS (TAG_NAME) VALUES (?)";
    private static final String SQL_SELECT_TAG = "SELECT TAG_ID, TAG_NAME FROM TAGS WHERE TAG_ID=?";
    private static final String SQL_DELETE_TAG = "DELETE FROM TAGS WHERE TAG_ID = ? ";
    private static final String SQL_UPDATE_TAG = "UPDATE TAGS SET TAG_NAME = ? WHERE TAG_ID = ? ";
    private static final String SQL_SELECT_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAGS ORDER BY TAG_NAME ASC ";
    private static final String SQL_FIND_TAG_ID_BY_NEWS_ID = "SELECT NT_TAG_ID FROM NEWS_TAGS WHERE NT_NEWS_ID = ? ";
    private static final String SQL_FIND_TAG_BY_NAME = "SELECT TAG_ID, TAG_NAME FROM TAGS WHERE TAG_NAME = ? ";
    private static final String SQL_DISCONNECT_WITH_NEWS = "DELETE FROM NEWS_TAGS WHERE NT_TAG_ID = ? ";

    private static final String COLUMN_NAME_TAG_NAME = "TAG_NAME";
    private static final String COLUMN_NAME_TAG_ID = "TAG_ID";
    private static final String COLUMN_NAME_NT_TAG_ID = "NT_TAG_ID";


    @Autowired
    private DataSource dataSource;

    @Override
    public Long create(Tag tag) throws IDAOException {

        Long tagId = null;
        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_CREATE_TAG, new String[] {COLUMN_NAME_TAG_ID})) {
            MakeStatement.make(ps, tag.getName());
            countRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            tagId = rs.getLong(1);
        } catch (SQLException e) {
            throw new TagDAOException(e + "\nCan not create tag : " + tag);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new TagDAOException("Didn't create tag.");

        return tagId;
    }


    @Override
    public Tag read(Long tagId) throws IDAOException {

        Tag tag = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_TAG)) {
            MakeStatement.make(ps, tagId);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                tag = createTag(rs);
            }
        } catch (SQLException e) {
            throw new TagDAOException(e + "\nTag with id: " + tagId + " doesn't exist.");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return tag;
    }


    @Override
    public List<Tag> readAll() throws IDAOException {

        List<Tag> tagList = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL_TAGS)) {
            ResultSet rs = ps.executeQuery();
            tagList = new ArrayList<>();
            while( rs.next() ){
                tagList.add(createTag(rs));
            }
        } catch (SQLException e) {
            throw new TagDAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tagList;
    }


    @Override
    public void update(Long tagId, Tag tag) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_TAG)) {
            MakeStatement.make(ps, tag.getName(), tag.getId());
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new TagDAOException(e + "\nCan not update tag with id : " + tagId + " to tag : " + tag);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new TagDAOException("Didn't update tag.");
    }


    @Override
    public void delete(Long tagId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_TAG)) {
            MakeStatement.make(ps, tagId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new TagDAOException(e + "\nCan not delete tag with id : " + tagId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new TagDAOException("Didn't delete tag.");

    }


    @Override
    public List<Long> findIdByNewsId(Long newsId) throws IDAOException {

        List<Long> arrayOfTagId = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_TAG_ID_BY_NEWS_ID)) {
            MakeStatement.make(ps, newsId);
            ResultSet rs = ps.executeQuery();
            arrayOfTagId = new ArrayList<>();
            while (rs.next()) {
                arrayOfTagId.add(rs.getLong(COLUMN_NAME_NT_TAG_ID));
            }
        } catch (SQLException e) {
            throw new TagDAOException(e + "\nCan not find tag by news id : " + newsId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return arrayOfTagId;
    }


    @Override
    public Tag findByName(String tagName) throws IDAOException {

        Tag tag = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_TAG_BY_NAME)) {
            MakeStatement.make(ps, tagName);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                tag = createTag(rs);
            }
        } catch (SQLException e) {
            throw new TagDAOException(e + "\nCan not find tag by name : " + tagName);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return tag;
    }


    @Override
    public void disconnectWithNews(Long tagId) throws IDAOException {

        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DISCONNECT_WITH_NEWS)) {
            MakeStatement.make(ps, tagId);
            ResultSet rs = ps.executeQuery();
        } catch (SQLException e) {
            throw new TagDAOException(e + "\nCan not disconnect tag with id : " + tagId + " with all news");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Create entity Tag from ResultSet
     * @param rs
     * @return entity Tag
     * @throws SQLException
     */
    private Tag createTag(ResultSet rs) throws SQLException {
        return new Tag(rs.getLong(COLUMN_NAME_TAG_ID), rs.getString(COLUMN_NAME_TAG_NAME));
    }

}
