package by.epam.lab.newsmanagment.dao.impl.oracle;

import by.epam.lab.newsmanagment.dao.ICommentDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.exception.dao.impl.CommentDAOException;
import by.epam.lab.newsmanagment.domain.Comment;
import by.epam.lab.newsmanagment.utils.MakeStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.ICommentDAO
 */
@Component
public class CommentDAO implements ICommentDAO {

    private static final String SQL_CREATE_COMMENT = " INSERT INTO COMMENTS ( COM_NEWS_ID, COM_TEXT, COM_CREATION_DATE) VALUES (?, ?, ?) ";
    private static final String SQL_SELECT_COMMENT = " SELECT COM_ID, COM_NEWS_ID, COM_TEXT, COM_CREATION_DATE FROM COMMENTS WHERE COM_ID = ? ";
    private static final String SQL_UPDATE_COMMENT = " UPDATE COMMENTS SET COM_TEXT = ?, COM_CREATION_DATE = ?  WHERE COM_ID = ? ";
    private static final String SQL_DELETE_COMMENT = " DELETE FROM COMMENTS WHERE COM_ID = ? ";
    private static final String SQL_SELECT_ALL_COMMENTS = " SELECT COM_ID, COM_NEWS_ID, COM_TEXT, COM_CREATION_DATE FROM COMMENTS ";
    private static final String SQL_FIND_COMMENTS_ID_BY_NEWS_ID = " SELECT COM_ID FROM COMMENTS WHERE COM_NEWS_ID = ? ";

    private static final String COLUMN_NAME_COMMENT_ID = "COM_ID";
    private static final String COLUMN_NAME_COMMENT_NEWS_ID = "COM_NEWS_ID";
    private static final String COLUMN_NAME_COMMENT_TEXT = "COM_TEXT";
    private static final String COLUMN_NAME_COMMENT_CREATION_DATE = "COM_CREATION_DATE";

    @Autowired
    private DataSource dataSource;


    @Override
    public Long create(Comment comment) throws IDAOException {

        Long comId = null;
        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_CREATE_COMMENT, new String[] {COLUMN_NAME_COMMENT_ID})) {
            MakeStatement.make(ps, comment.getNewsId(), comment.getComText(), comment.getComCreationDate());
            countRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            comId = rs.getLong(1);
        } catch (SQLException e) {
            throw new CommentDAOException(e + "\nCan not create comment : " + comment);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new CommentDAOException("Didn't delete comment.");

        return comId;
    }


    @Override
    public Comment read(Long comId) throws IDAOException {

        Comment comment = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_COMMENT)) {
            MakeStatement.make(ps, comId);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                comment = createComment(rs);
            }
        } catch (SQLException e) {
            throw new CommentDAOException(e + "\nComment with id: " + comId + " doesn't exist.");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return comment;
    }


    @Override
    public List<Comment> readAll() throws IDAOException {

        List<Comment> commentList = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL_COMMENTS)) {
            ResultSet rs = ps.executeQuery();
            commentList = new ArrayList<>();
            while( rs.next() ){
                commentList.add(createComment(rs));
            }
        } catch (SQLException e) {
            throw new CommentDAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return commentList;
    }


    @Override
    public void update(Long comId, Comment comment) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_COMMENT)) {
            MakeStatement.make(ps, comment.getComText(), comment.getComCreationDate(), comId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new CommentDAOException(e + "\nCan not update comment with id : " + comId + " to comment : " + comment);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new CommentDAOException("Didn't update comment.");
    }


    @Override
    public void delete(Long comId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_COMMENT)) {
            MakeStatement.make(ps, comId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new CommentDAOException(e + "\nCan not delete comment with id : " + comId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new CommentDAOException("Didn't delete comment.");
    }


    @Override
    public List<Long> findIdByNewsId(Long newsId) throws IDAOException {

        List<Long> commentList = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_COMMENTS_ID_BY_NEWS_ID)) {
            MakeStatement.make(ps, newsId);
            ResultSet rs = ps.executeQuery();
            commentList = new ArrayList<>();
            while( rs.next() ){
                commentList.add(rs.getLong(COLUMN_NAME_COMMENT_ID));
            }
        } catch (SQLException e) {
            throw new CommentDAOException(e + "\nCan not find comments by news with id : " + newsId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return commentList;
    }

    /**
     * Create entity Comment from ResultSet
     * @param rs
     * @return entity Comment
     * @throws SQLException
     */
    private Comment createComment (ResultSet rs) throws SQLException {
        Long comId = rs.getLong(COLUMN_NAME_COMMENT_ID);
        Long newsId = rs.getLong(COLUMN_NAME_COMMENT_NEWS_ID);
        String comText = rs.getString(COLUMN_NAME_COMMENT_TEXT);
        Timestamp comCreationDate = rs.getTimestamp(COLUMN_NAME_COMMENT_CREATION_DATE);
        return new Comment(comId, newsId, comText, comCreationDate);
    }
}
