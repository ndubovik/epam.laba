package by.epam.lab.newsmanagment.exception.dao.impl;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception in CommentDAO
 */
public class CommentDAOException extends IDAOException {
    public CommentDAOException(){}
    public CommentDAOException(String message, Throwable exception) {
        super(message, exception);
    }
    public CommentDAOException(String message) {
        super(message);
    }
    public CommentDAOException(Throwable exception) {
        super(exception);
    }
}
