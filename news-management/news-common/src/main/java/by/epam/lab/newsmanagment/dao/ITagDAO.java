package by.epam.lab.newsmanagment.dao;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Tag;

import java.util.List;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.GenericDAO
 */
public interface ITagDAO extends GenericDAO<Long, Tag> {

    /**
     * Find all tag id by news id.
     * That means that we select all tags for the news.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return list of tag id
     */
    List<Long> findIdByNewsId(Long newsId) throws IDAOException;

    /**
     * Find tag by name
     * @param name
     * @return tag with required name
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
    Tag findByName(String name) throws IDAOException;

    /**
     * Disconnect tag with author
     * @param tagId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
    void disconnectWithNews(Long tagId) throws IDAOException;
}
