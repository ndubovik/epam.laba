package by.epam.lab.newsmanagment.exception.dao.impl;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception in AuthorDAO
 */
public class AuthorDAOException extends IDAOException {
    public AuthorDAOException(){}
    public AuthorDAOException(String message, Throwable exception) {
        super(message, exception);
    }
    public AuthorDAOException(String message) {
        super(message);
    }
    public AuthorDAOException(Throwable exception) {
        super(exception);
    }
}
