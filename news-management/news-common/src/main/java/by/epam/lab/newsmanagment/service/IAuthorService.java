package by.epam.lab.newsmanagment.service;

import by.epam.lab.newsmanagment.domain.Author;
import by.epam.lab.newsmanagment.domain.AuthorRecord;
import by.epam.lab.newsmanagment.exception.service.IServiceException;

import java.util.List;

/**
 * @author Nastya Dubovik
 */
public interface IAuthorService {

    /**
     * Get all authors
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return list of active authors
     */
    List<Author> getActiveAuthors() throws IServiceException;

    /**
     * Add author to database
     * @param author
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return id of row that has been created
     */
    Long add (Author author) throws IServiceException;

    /**
     * This method makes the author expired..
     * @param author
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    void makeExpired(Author author) throws IServiceException;

    /**
     * Get author by news id.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return author
     */
    Author getByNewsId(Long newsId) throws IServiceException;

    /**
     * Get author by its id.
     * @param authorId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return author
     */
    Author getById(Long authorId) throws IServiceException;

    /**
     * Get all authors.
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return list of authors
     */
    List<Author> getAll() throws IServiceException;

    /**
     * Edit author.
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return list of authors
     */
    void edit(AuthorRecord authorRecord) throws IServiceException;
}
