package by.epam.lab.newsmanagment.service.impl;

import by.epam.lab.newsmanagment.dao.IRoleDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Role;
import by.epam.lab.newsmanagment.service.IRoleService;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.service.IRoleService
 */
@Service("roleService")
public class RoleService implements IRoleService {

    @Autowired
    private IRoleDAO roleDAO;

    @Override
    public Long add(Role role) throws IServiceException {
        Long id = null;
        try {
            id = roleDAO.create(role);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in RoleService", e);
        }
        return id;
    }

    @Override
    public void edit(Long roleId, Role role) throws IServiceException {
        try {
            roleDAO.update(roleId, role);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in RoleService", e);
        }
    }

    @Override
    public void delete(Role role) throws IServiceException {
        try {
            roleDAO.delete(role.getRoleId());
        } catch (IDAOException e) {
            throw new IServiceException("Exception in RoleService", e);
        }
    }

    @Override
    public Role get(Long roleId) throws IServiceException {
        Role role = null;
        try {
            role = roleDAO.read(roleId);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in RoleService", e);
        }
        return role;
    }

    @Override
    public Long findIdByUserId(Long userId) throws IServiceException {
        Long roleId = null;
        try {
            roleId = roleDAO.findIdByUserId(userId);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in RoleService", e);
        }
        return roleId;
    }
}
