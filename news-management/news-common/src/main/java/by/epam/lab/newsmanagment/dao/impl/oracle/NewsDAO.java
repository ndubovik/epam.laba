package by.epam.lab.newsmanagment.dao.impl.oracle;

import by.epam.lab.newsmanagment.dao.INewsDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.exception.dao.impl.NewsDAOException;
import by.epam.lab.newsmanagment.domain.News;
import by.epam.lab.newsmanagment.exception.dao.impl.UserDAOException;
import by.epam.lab.newsmanagment.utils.MakeStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.INewsDAO
 */
@Component
public class NewsDAO implements INewsDAO {

    private static final String SQL_CREATE_NEWS = " INSERT INTO NEWS ( NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, " +
            " NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES (?, ?, ?, ?, ?) ";
    private static final String SQL_SELECT_NEWS = " SELECT NEWS_ID, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, " +
            " NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ? ";
    private static final String SQL_SELECT_ALL_NEWS = " SELECT NEWS_ID, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, " +
            " NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE FROM NEWS  ";

    private static final String[] SQL_SELECT_ALL_NEWS_BY_CRITERIA_WITH_PAGINATION = {

    /* 0 */         " SELECT NEWS_ID, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, " +
                    " NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE, COM_AMOUNT, RN " +
                    " FROM ( " +
                    " SELECT NEWS_ID, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, " +
                    " NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE, COUNT(COM_ID) COM_AMOUNT," +
                    " ROW_NUMBER() OVER (ORDER BY NEWS.NEWS_MODIFICATION_DATE DESC, COUNT(COM_ID) DESC) RN " +
                    " FROM NEWS " +
                    " LEFT JOIN COMMENTS " +
                    " ON NEWS_ID = COM_NEWS_ID ",


    /* 1  */        " LEFT JOIN NEWS_AUTHORS " +
                    " ON NEWS_ID = NA_NEWS_ID " +
                    " LEFT JOIN NEWS_TAGS " +
                    " ON NEWS_ID = NT_NEWS_ID " +
                    " WHERE ",

    /* 2 */         " NA_AUTHOR_ID IN " ,

    /* 3 */         " ( ",

    /* 4 */         " ) ",

    /* 5 */         " AND ",

    /* 6 */         " NT_TAG_ID IN ",

    /* 7 */         " GROUP BY NEWS_ID, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, " +
                    " NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE ) " +
                    " WHERE rn BETWEEN "

    };

    private static final String[] SQL_SELECT_ALL_NEWS_BY_CRITERIA = {

    /* 0 */     " SELECT COUNT(DISTINCT NEWS_ID) NEWS_AMOUNT FROM NEWS ",

    /* 1 */     " LEFT JOIN NEWS_AUTHORS " +
                " ON NEWS_ID = NA_NEWS_ID " +
                " LEFT JOIN NEWS_TAGS " +
                " ON NEWS_ID = NT_NEWS_ID " +
                " WHERE ",

    /* 2 */     " NA_AUTHOR_ID IN ",

    /* 3 */      " ( ",

    /* 4 */      " ) ",

    /* 5 */      " AND ",

    /* 6 */      " NT_TAG_ID IN  "

    };

    private static final String SQL_UPDATE_NEWS = " UPDATE NEWS SET NEWS_TITLE = ?, NEWS_SHORT_TEXT = ?, NEWS_FULL_TEXT = ?, " +
            " NEWS_MODIFICATION_DATE = ? WHERE NEWS_ID = ? ";
    private static String SQL_DELETE_NEWS = " DELETE FROM NEWS WHERE NEWS_ID = ? ";

    private static final String SQL_CONNECT_NEWS_TAG = " INSERT INTO NEWS_TAGS (NT_NEWS_ID, NT_TAG_ID) VALUES (?, ?) ";
    private static final String SQL_CONNECT_NEWS_AUTHOR = " INSERT INTO NEWS_AUTHORS (NA_NEWS_ID, NA_AUTHOR_ID) VALUES (?, ?) ";
    private static final String SQL_DISCONNECT_NEWS_TAG = " DELETE FROM NEWS_TAGS WHERE NT_NEWS_ID = ? AND NT_TAG_ID = ? ";
    private static final String SQL_DISCONNECT_NEWS_TAGS = " DELETE FROM NEWS_TAGS WHERE NT_NEWS_ID = ? ";
    private static final String SQL_DISCONNECT_NEWS_AUTHOR = " DELETE FROM NEWS_AUTHORS WHERE NA_NEWS_ID = ? ";

    private static final String COLUMN_NAME_NEWS_ID = "NEWS_ID";
    private static final String COLUMN_NAME_NEWS_TITLE = "NEWS_TITLE";
    private static final String COLUMN_NAME_NEWS_SHORT_TEXT = "NEWS_SHORT_TEXT";
    private static final String COLUMN_NAME_NEWS_FULL_TEXT = "NEWS_FULL_TEXT";
    private static final String COLUMN_NAME_NEWS_CREATION_DATE = "NEWS_CREATION_DATE";
    private static final String COLUMN_NAME_NEWS_MODIFICATION_DATE = "NEWS_MODIFICATION_DATE";
    private static final String COLUMN_NAME_NEWS_AMOUNT = "NEWS_AMOUNT";

    @Autowired
    private DataSource dataSource;


    @Override
    public Long create(News news) throws IDAOException {

        Long newsId = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        int countRows = 0;
        try (PreparedStatement ps = connection.prepareStatement(SQL_CREATE_NEWS, new String[] {COLUMN_NAME_NEWS_ID})) {
            MakeStatement.make(ps, news.getTitle(), news.getShortText(),
                    news.getFullText(), news.getCreationDate(), news.getModificationDate());
            countRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            newsId = rs.getLong(1);
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not create news : " + news);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new NewsDAOException("Didn't create news.");

        return newsId;
    }


    @Override
    public News read(Long newsId) throws IDAOException {

        News news = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_NEWS)) {
            MakeStatement.make(ps, newsId);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                news = createNews(rs);
            }
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nNews with id: " + newsId + " doesn't exist.");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return news;
    }


    @Override
    public List<News> readAll() throws IDAOException {

        List<News> newsList = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL_NEWS)) {
            ResultSet rs = ps.executeQuery();
            newsList = new ArrayList<>();
            while( rs.next() ){
                newsList.add(createNews(rs));
            }
        } catch (SQLException e) {
            throw new NewsDAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return newsList;
    }

    @Override
    public List<Long> readAll(Set<Long> tagIdSet, Set<Long> authorIdSet,
                              int newsPerPage, int page) throws IDAOException {

        List<Long> newsIdList = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        Integer startNewsNumber = (page - 1) * newsPerPage + 1;
        Integer endNewsNumber = page * newsPerPage;
        String searchCriteriaQueryWithPagination = prepareCriteriaQueryWithPagination(tagIdSet, authorIdSet, startNewsNumber, endNewsNumber);
        try (PreparedStatement ps = connection.prepareStatement(searchCriteriaQueryWithPagination)) {
            ResultSet rs = ps.executeQuery();
            newsIdList = new LinkedList <>();
            Long newsId = null;
            while( rs.next() ){
                newsId = rs.getLong(COLUMN_NAME_NEWS_ID);
                newsIdList.add(newsId);
            }
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not read news by tagIdSet : " + tagIdSet + " and authorIdSet : " + authorIdSet + " for page : " + page);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return newsIdList;
    }


    @Override
    public void update(Long newsId, News news) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_NEWS)) {
            MakeStatement.make(ps, news.getTitle(), news.getShortText(), news.getFullText(),
                    news.getModificationDate(), newsId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not update news with id : " + newsId + " to news : " + news);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        if(countRows == 0)
            throw new NewsDAOException("Didn't update news.");
    }


    @Override
    public void delete(Long newsId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_NEWS)) {
            MakeStatement.make(ps, newsId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not delete news with id : " + newsId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new UserDAOException("Didn't delete news.");
    }


    @Override
    public void connectWithAuthor(Long newsId, Long authorId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_CONNECT_NEWS_AUTHOR)) {
            MakeStatement.make(ps, newsId, authorId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not connect news with id : " + newsId + " with author with id : " + authorId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new UserDAOException("Didn't connect news with author.");

    }


    @Override
    public void connectWithTag(Long newsId, Long tagId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_CONNECT_NEWS_TAG)) {
            MakeStatement.make(ps, newsId, tagId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not connect news with id : " + newsId + " with tag with id : " + tagId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new NewsDAOException("Didn't connect news with tag.");
    }


    @Override
    public void disconnectWithAuthor(Long newsId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DISCONNECT_NEWS_AUTHOR)) {
            MakeStatement.make(ps, newsId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not disconnect news with id : " + newsId + " with author");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new NewsDAOException("Didn't disconnect news with author.");
    }


    @Override
    public void disconnectWithTag(Long newsId, Long tagId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DISCONNECT_NEWS_TAG)) {
            MakeStatement.make(ps, newsId, tagId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not disconnect news with id : " + newsId + " with tag with id : " + tagId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new NewsDAOException("Didn't disconnect news with tag.");
    }


    @Override
    public void disconnectWithTags (Long newsId) throws IDAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DISCONNECT_NEWS_TAGS)) {
            MakeStatement.make(ps, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not disconnect news with id : " + newsId + " with tags");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
    }

    @Override
    public int countNews(Set<Long> tagIdSet, Set<Long> authorIdSet) throws IDAOException {
        int count = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String query = prepareCountQuery(tagIdSet, authorIdSet);
        try (PreparedStatement ps = connection.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            if(rs.next()){
                count = rs.getInt(COLUMN_NAME_NEWS_AMOUNT);
            }
        } catch (SQLException e) {
            throw new NewsDAOException(e + "\nCan not count news by tafIdSet : " + tagIdSet + " and authorIdSet : " + authorIdSet);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return count;
    }

    /**
     * Create entity News from ResultSet.
     * @param rs
     * @return entity News
     * @throws SQLException
     */
    private News createNews (ResultSet rs) throws SQLException {
        Long newsId = rs.getLong(COLUMN_NAME_NEWS_ID);
        String title = rs.getString(COLUMN_NAME_NEWS_TITLE);
        String shortText = rs.getString(COLUMN_NAME_NEWS_SHORT_TEXT);
        String fullText = rs.getString(COLUMN_NAME_NEWS_FULL_TEXT);
        LocalDateTime creationDate = rs.getTimestamp(COLUMN_NAME_NEWS_CREATION_DATE).toLocalDateTime();
        LocalDate modificationDate = rs.getTimestamp(COLUMN_NAME_NEWS_MODIFICATION_DATE).toLocalDateTime().toLocalDate();
        return new News(newsId, title, shortText, fullText, creationDate, modificationDate);
    }

    /**
     * Create query to extract all news that have the same author and tags on defined page
     * @param tagIdSet
     * @param authorIdSet
     * @param startNewsNumber
     * @param endNewsNumber
     * @return query string
     */
    private String prepareCriteriaQueryWithPagination(Set<Long> tagIdSet, Set<Long> authorIdSet, Integer startNewsNumber, Integer endNewsNumber){
        StringBuffer sb = new StringBuffer();
        sb.append(SQL_SELECT_ALL_NEWS_BY_CRITERIA_WITH_PAGINATION[0]);
        setJoinClause(sb, SQL_SELECT_ALL_NEWS_BY_CRITERIA_WITH_PAGINATION, tagIdSet, authorIdSet);
        sb.append(SQL_SELECT_ALL_NEWS_BY_CRITERIA_WITH_PAGINATION[7]);
        sb.append(startNewsNumber);
        sb.append(SQL_SELECT_ALL_NEWS_BY_CRITERIA_WITH_PAGINATION[5]);
        sb.append(endNewsNumber);
        return sb.toString();
    }

    /**
     * Create query to count news that have the same author and tags
     * @param tagIdSet
     * @param authorIdSet
     * @return
     */
    private String prepareCountQuery(Set<Long> tagIdSet, Set<Long> authorIdSet){
        StringBuffer sb = new StringBuffer();
        sb.append(SQL_SELECT_ALL_NEWS_BY_CRITERIA[0]);
        setJoinClause(sb, SQL_SELECT_ALL_NEWS_BY_CRITERIA, tagIdSet, authorIdSet);
        return sb.toString();
    }

    /**
     * Set JOIN clause with NEWS_AUTHORS and  NEWS_TAGS in query string
     * @param sb
     * @param SQL_QUERY_PART
     * @param tagIdSet
     * @param authorIdSet
     */
    private void setJoinClause(StringBuffer sb, String[] SQL_QUERY_PART, Set<Long> tagIdSet, Set<Long> authorIdSet){

        if( ( authorIdSet != null && !authorIdSet.isEmpty() ) ||  ( tagIdSet != null && !tagIdSet.isEmpty() ) ){
            sb.append(SQL_QUERY_PART[1]);
            if( authorIdSet != null && !authorIdSet.isEmpty() ){
                sb.append(SQL_QUERY_PART[2]);
                sb.append(SQL_QUERY_PART[3]);
                for(Long authorId : authorIdSet){
                    sb.append(authorId);
                    sb.append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
                sb.append(SQL_QUERY_PART[4]);
                if( tagIdSet != null && !tagIdSet.isEmpty() ){
                    sb.append(SQL_QUERY_PART[5]);
                }
            }
        }
        if( tagIdSet != null && !tagIdSet.isEmpty() ){
            sb.append(SQL_QUERY_PART[6]);
            sb.append(SQL_QUERY_PART[3]);
            for(Long tagId : tagIdSet){
                sb.append(tagId);
                sb.append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(SQL_QUERY_PART[4]);
        }

    }


}
