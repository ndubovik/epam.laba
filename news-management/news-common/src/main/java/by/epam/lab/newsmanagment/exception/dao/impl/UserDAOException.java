package by.epam.lab.newsmanagment.exception.dao.impl;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception in UserDAO
 */
public class UserDAOException extends IDAOException {
    public UserDAOException(){}
    public UserDAOException(String message, Throwable exception) {
        super(message, exception);
    }
    public UserDAOException(String message) {
        super(message);
    }
    public UserDAOException(Throwable exception) {
        super(exception);
    }
}
