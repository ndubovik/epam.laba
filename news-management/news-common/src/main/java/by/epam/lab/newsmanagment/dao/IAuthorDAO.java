package by.epam.lab.newsmanagment.dao;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Author;



/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.GenericDAO
 */
public interface IAuthorDAO extends GenericDAO<Long, Author> {

    /**
     * Find author id by his name.
     * @param name
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return author id
     */
    Long findId(String name) throws IDAOException;

    /**
     * Find author id that is connected with news.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return author id
     */
    Long findIdByNewsId(Long newsId) throws IDAOException;
}
