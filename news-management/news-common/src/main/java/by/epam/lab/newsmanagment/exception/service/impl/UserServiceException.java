package by.epam.lab.newsmanagment.exception.service.impl;

import by.epam.lab.newsmanagment.exception.service.IServiceException;

/**
 * Created by Настенька on 3/14/2016.
 */
public class UserServiceException extends IServiceException {
    public UserServiceException(){}
    public UserServiceException(String message, Throwable exception) {
        super(message, exception);
    }
    public UserServiceException(String message) {
        super(message);
    }
    public UserServiceException(Throwable exception) {
        super(exception);
    }
}
