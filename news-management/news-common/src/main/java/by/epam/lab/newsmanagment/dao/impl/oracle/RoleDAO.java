package by.epam.lab.newsmanagment.dao.impl.oracle;

import by.epam.lab.newsmanagment.dao.IRoleDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.exception.dao.impl.RoleDAOException;
import by.epam.lab.newsmanagment.domain.Role;
import by.epam.lab.newsmanagment.domain.RoleName;
import by.epam.lab.newsmanagment.utils.MakeStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.IRoleDAO
 */
@Component
public class RoleDAO implements IRoleDAO {

    private static final String SQL_CREATE_ROLE = " INSERT INTO ROLES (RLS_USER_ID, RLS_NAME) VALUES (?, ?) ";
    private static final String SQL_SELECT_ROLE = " SELECT RLS_ID, RLS_USER_ID, RLS_NAME FROM ROLES WHERE RLS_ID = ? ";
    private static final String SQL_UPDATE_ROLE = " UPDATE ROLES SET RLS_NAME = ? WHERE RLS_ID = ? ";
    private static final String SQL_DELETE_ROLE = " DELETE FROM ROLES WHERE RLS_ID = ? ";
    private static final String SQL_SELECT_ALL_ROLES = " SELECT RLS_ID, RLS_USER_ID, RLS_NAME FROM ROLES ";
    private static final String SQL_FIND_ROLE_ID_BY_USER_ID = "SELECT RLS_ID FROM ROLES WHERE RLS_USER_ID = ?";

    private static final String COLUMN_NAME_ROLE_NAME = "RLS_NAME";
    private static final String COLUMN_NAME_USER_ID = "RLS_USER_ID";
    private static final String COLUMN_NAME_ROLE_ID = "RLS_ID";

    @Autowired
    private DataSource dataSource;


    @Override
    public Long create(Role role) throws IDAOException {

        Long roleId = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        int countRows = 0;
        try (PreparedStatement ps = connection.prepareStatement(SQL_CREATE_ROLE, new String[] {COLUMN_NAME_ROLE_ID})) {
            MakeStatement.make(ps, role.getUserId(), role.getRoleName());
            countRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            roleId = rs.getLong(1);
        } catch (SQLException e) {
            throw new RoleDAOException(e + "\nCan not create role : " + role);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new RoleDAOException("Didn't create role.");

        return roleId;
    }


    @Override
    public Role read(Long roleId) throws IDAOException {

        Role role = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ROLE)) {
            MakeStatement.make(ps, roleId);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                role = createRole(rs);
            }
        } catch (SQLException e) {
            throw new RoleDAOException(e + "\nRole with id: " + roleId + " doesn't exist.");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return role;
    }


    @Override
    public List<Role> readAll() throws IDAOException {

        List<Role> roleList = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL_ROLES);
             ResultSet rs = ps.executeQuery()) {
            roleList = new ArrayList<>();
            while( rs.next() ){
                roleList.add(createRole(rs));
            }
        } catch (SQLException e) {
            throw new RoleDAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return roleList;
    }


    @Override
    public void update(Long roleId, Role role) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_ROLE)) {
            MakeStatement.make(ps, role.getRoleName(), roleId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new RoleDAOException(e + "\nCan not update role with id : " + roleId + " to role : " + role);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new RoleDAOException("Didn't update role.");
    }


    @Override
    public void delete(Long roleId) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_ROLE)) {
            MakeStatement.make(ps, roleId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new RoleDAOException(e + "\nCan not delete role with id : " + roleId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new RoleDAOException("Didn't delete role.");
    }


    @Override
    public Long findIdByUserId(Long userId) throws IDAOException {

        Long roleId = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_ROLE_ID_BY_USER_ID)) {
            MakeStatement.make(ps, userId);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                roleId = rs.getLong(COLUMN_NAME_ROLE_ID);
            }
        } catch (SQLException e) {
            throw new RoleDAOException(e + "\nRole with such userId: " + userId + " doesn't exist.");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return roleId;
    }

    /**
     * Create entity Role from resultSet
     * @param rs
     * @return entity Role
     * @throws SQLException
     */
    private Role createRole (ResultSet rs) throws SQLException {

        Role role = new Role();
        role.setRoleId( rs.getLong(COLUMN_NAME_ROLE_ID) );
        role.setUserId( rs.getLong(COLUMN_NAME_USER_ID) );
        RoleName roleName = RoleName.valueOf(rs.getString(COLUMN_NAME_ROLE_NAME));
        role.setRoleName( roleName );
        return role;
    }

}
