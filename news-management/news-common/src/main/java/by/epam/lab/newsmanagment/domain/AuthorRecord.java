package by.epam.lab.newsmanagment.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AuthorRecord {

    private Long id;
    @NotNull
    @Size(min=1, max=30)
    private String name;
    private Boolean isExpired;

    public AuthorRecord() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(Boolean expired) {
        isExpired = expired;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("AuthorRecord: ");
        sb.append("id=" + id);
        sb.append(" , name=" + name);
        sb.append(" , isExpired=" + isExpired);
        return sb.toString();
    }
}
