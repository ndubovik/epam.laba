package by.epam.lab.newsmanagment.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Nastya Dubovik
 */
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long comId;
    private Long newsId;

    @NotNull
    @Size(min=1, max=100)
    private String comText;

    private Timestamp comCreationDate;

    public Comment() {
        this.comId = null;
        this.newsId = null;
        this.comText = null;
        this.comCreationDate = null;
    }

    public Comment(Long newsId, String comText) {
        this.comId = null;
        this.newsId = newsId;
        this.comText = comText;
        this.comCreationDate = new Timestamp(System.currentTimeMillis());
    }

    public Comment(Long newsId, String comText, Timestamp comCreateionDate) {
        this.comId = null;
        this.newsId = newsId;
        this.comText = comText;
        this.comCreationDate = comCreateionDate;
    }

    public Comment(Long comId, Long newsId, String comText) {
        this.comId = comId;
        this.newsId = newsId;
        this.comText = comText;
        this.comCreationDate = new Timestamp(System.currentTimeMillis());
    }

    public Comment(Long comId, Long newsId, String comText, Timestamp comCreationDate) {
        this.comId = comId;
        this.newsId = newsId;
        this.comText = comText;
        this.comCreationDate = comCreationDate;
    }

    public Long getComId() {
        return comId;
    }

    public void setComId(Long comId) {
        this.comId = comId;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getComText() {
        return comText;
    }

    public void setComText(String comText) {
        this.comText = comText;
    }

    public Timestamp getComCreationDate() {
        return comCreationDate;
    }

    public void setComCreationDate(Timestamp comCreationDate) {
        this.comCreationDate = comCreationDate;
    }

    /**
     * Compare this object with object that are sent
     * @param obj
     * @return true if this object equals to obj, false if this object doesn't equal to obj
     */
    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Comment o = (Comment)obj;
        if( newsId != o.newsId ){
            return false;
        }
        if(! this.comText.equals(o.comText)){
            return false;
        }
        if(! this.comCreationDate.equals(o.comCreationDate)){
            return false;
        }
        return true;
    }

    /**
     * Create a string that contains all information about the object
     * @return string with full information
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": comId = " + comId);
        sb.append(", newsId = " + newsId);
        sb.append(", comText = " + comText);
        sb.append(", comCreationDate = " + comCreationDate);
        return sb.toString();
    }

    /**
     * Depending on main fields creat a hash code
     * @return hash code
     */
    @Override
    public int hashCode(){
        int hash = (newsId != null)? newsId.hashCode()*31 : 0 + comText.hashCode()*31 + comCreationDate.hashCode()*31 ;
        return hash;
    }
}
