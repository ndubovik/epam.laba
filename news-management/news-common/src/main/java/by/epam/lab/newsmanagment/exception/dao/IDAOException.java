package by.epam.lab.newsmanagment.exception.dao;


/**
 * @author Nastya Dubovik
 * Describe an exception in DAO
 */
public class IDAOException extends Exception {
	public IDAOException(){}
    public IDAOException(String message, Throwable exception) {
        super(message, exception);
    }
    public IDAOException(String message) {
        super(message);
    }
    public IDAOException(Throwable exception) {
        super(exception);
    }
}
