package by.epam.lab.newsmanagment.service.impl;

import by.epam.lab.newsmanagment.domain.Role;
import by.epam.lab.newsmanagment.domain.dto.UserTO;
import by.epam.lab.newsmanagment.exception.dao.impl.NoSuchEntityException;
import by.epam.lab.newsmanagment.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.User;
import by.epam.lab.newsmanagment.service.IUserService;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import by.epam.lab.newsmanagment.dao.IUserDAO;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.service.IUserService
 */
@Service("userService")
public class UserService implements IUserService {
	
	@Autowired
	private IUserDAO userDAO;

	@Autowired
	private IRoleService roleService;

	@Transactional(rollbackFor = Exception.class)
	@Override
	public UserTO registration(UserTO userTO) throws IServiceException {
		User user = userTO.getUser();
		String login = user.getLogin();
		try {
			if( checkUser(login) ){
				Long userId = userDAO.create(user);
				user.setId(userId);
				Role role = userTO.getRole();
				role.setUserId(userId);
				Long roleId = roleService.add(role);
				role.setRoleId(roleId);
			}
			else {
				throw new IServiceException("Login: " + login + " isn't free.");
			}
		} catch (IDAOException e) {
			throw new IServiceException("Exception in UserService", e);
		} 
		return userTO;
	}


	@Override
	public UserTO login (String login, String password) throws IServiceException {
		UserTO userTO = null;
		try {
			Long userId = userDAO.findId(login);
			User userExpected = userDAO.read(userId);
			if (password.equals(userExpected.getPassword())) {
				Long roleId = roleService.findIdByUserId(userExpected.getId());
				Role userRole = roleService.get(roleId);
				userTO = new UserTO(userExpected, userRole);
			} else {
				throw new IServiceException("Password: " + password + " is not correct");
			}

		} catch (NoSuchEntityException e){
			throw new IServiceException("The user " + login + " wasn't registered", e);
		} catch (IDAOException e) {
			throw new IServiceException("Exception in UserService", e);
		}

		return userTO;
	}


	@Transactional(rollbackFor = Exception.class)
	@Override
	public void delete (UserTO userTO) throws IServiceException {
		User user = userTO.getUser();
		Role role = userTO.getRole();
		try {
			userDAO.delete(user.getId());
			roleService.delete(role);
		} catch (IDAOException e) {
			throw new IServiceException("Exception in UserService", e);
		}
	}


	@Transactional(rollbackFor = Exception.class)
	@Override
	public void edit (UserTO userTO) throws IServiceException {
		User user = userTO.getUser();
		Role role = userTO.getRole();
		try {
			userDAO.update(user.getId(), user);
			roleService.edit(role.getRoleId(), role);
		} catch (IDAOException e) {
			throw new IServiceException("Exception in UserService", e);
		}
	}

	/**
	 * Method check the user in the database by his login.
	 * If such user doesn't exist, DAO will throw NoSuchEntityException.
	 * That's why we catch this exception and decide what to return.
	 * @param login
	 * @return true if login is free, false if login is busy
	 * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
	private boolean checkUser (String login) throws IDAOException {
		try {
			userDAO.findId(login);
		} catch (NoSuchEntityException e) {
			return true;
		}
		return false;
	}
}
