package by.epam.lab.newsmanagment.exception.dao.impl;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception in NewsDAO
 */
public class NewsDAOException extends IDAOException {
    public NewsDAOException(){}
    public NewsDAOException(String message, Throwable exception) {
        super(message, exception);
    }
    public NewsDAOException(String message) {
        super(message);
    }
    public NewsDAOException(Throwable exception) {
        super(exception);
    }
}
