package by.epam.lab.newsmanagment.dao;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Comment;

import java.util.List;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.GenericDAO
 */
public interface ICommentDAO extends GenericDAO<Long, Comment> {

    /**
     * Find all comments id that are connected with news.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return list of comments id
     */
    List<Long> findIdByNewsId(Long newsId) throws IDAOException;
}
