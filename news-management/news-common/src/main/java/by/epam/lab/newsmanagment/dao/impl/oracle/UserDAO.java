package by.epam.lab.newsmanagment.dao.impl.oracle;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import by.epam.lab.newsmanagment.dao.IUserDAO;
import by.epam.lab.newsmanagment.exception.dao.impl.UserDAOException;
import by.epam.lab.newsmanagment.utils.MakeStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.User;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.IUserDAO
 */
@Component
public class UserDAO implements IUserDAO {
	
	private static final String SQL_CREATE_USER = "INSERT INTO USERS(USER_NAME, USER_LOGIN, USER_PASSWORD) VALUES(?,?,?)";
	private static final String SQL_SELECT_USER = "SELECT USER_ID, USER_NAME, USER_LOGIN, USER_PASSWORD FROM USERS " +
			" WHERE USER_ID=?";
	private static final String SQL_DELETE_USER = "DELETE FROM USERS WHERE USER_ID = ? ";
	private static final String SQL_FIND_USER_ID = "SELECT USER_ID FROM USERS WHERE USER_LOGIN = ? ";
	private static final String SQL_UPDATE_USER = "UPDATE USERS SET USER_NAME = ?, USER_LOGIN = ?, USER_PASSWORD = ? " +
			" WHERE USER_ID = ? ";
	private static final String SQL_SELECT_ALL_USERS = "SELECT USER_ID, USER_NAME, USER_LOGIN, USER_PASSWORD FROM USERS";

	private static final String COLUMN_NAME_USER_ID = "USER_ID";
	private static final String COLUMN_NAME_USER_NAME = "USER_NAME";
	private static final String COLUMN_NAME_USER_LOGIN = "USER_LOGIN";
	private static final String COLUMN_NAME_USER_PASSWORD = "USER_PASSWORD";

	@Autowired
	private DataSource dataSource;


	@Override
	public Long create(User user) throws IDAOException {

		Long userId = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		int countRows = 0;
		try (PreparedStatement ps = connection.prepareStatement(SQL_CREATE_USER, new String[] {COLUMN_NAME_USER_ID})) {
			MakeStatement.make(ps, user.getName(), user.getLogin(), user.getPassword());
			countRows = ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			userId = rs.getLong(1);
		} catch (SQLException e) {
			throw new UserDAOException(e + "\nCan not create user: " + user);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		/**
		 * Because: preparedStatement returns either the row count for
		 * SQL Data Manipulation Language (DML) statements or 0 for
		 * SQL statements that return nothing
		 */
		if(countRows == 0)
			throw new UserDAOException("Didn't create user.");

		return userId;
	}


	@Override
	public User read(Long userId) throws IDAOException {

		User user = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_USER)) {
			MakeStatement.make(ps, userId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				user = createUser(rs);
			}
		} catch (SQLException e) {
			throw new UserDAOException(e + "\nUser with id: " + userId + " doesn't exist.");
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return user;
	}


	@Override
	public List<User> readAll() throws IDAOException {

		List<User> userList = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL_USERS)) {
			ResultSet rs = ps.executeQuery();
			userList = new ArrayList<>();
			while( rs.next() ){
				userList.add( createUser(rs) );
			}
		} catch (SQLException e) {
			throw new UserDAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return userList;
	}


	@Override
	public void update(Long userId, User user) throws IDAOException {

		int countRows = 0;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_USER)) {
			MakeStatement.make(ps, user.getName(), user.getLogin(), user.getPassword(), userId);
			countRows = ps.executeUpdate();
		} catch (SQLException e) {
			throw new UserDAOException(e + "\nCan not update user with id : " + userId + " to user : " + user);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		if(countRows == 0)
			throw new UserDAOException("Didn't update user.");
	}


	@Override
	public void delete(Long userId) throws IDAOException {

		int countRows = 0;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_USER)) {
			MakeStatement.make(ps, userId);
			countRows = ps.executeUpdate();
		} catch (SQLException e) {
			throw new UserDAOException(e + "\nCan not delete user with id : " + userId);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		/**
		 * Because: preparedStatement returns either the row count for
		 * SQL Data Manipulation Language (DML) statements or 0 for
		 * SQL statements that return nothing
		 */
		if(countRows == 0)
			throw new UserDAOException("Didn't delete user.");

	}


	@Override
	public Long findId(String login) throws IDAOException {

		Long userId = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_USER_ID)) {
			MakeStatement.make(ps, login);
			ResultSet resultSet = ps.executeQuery();
			if( resultSet.next() ){
				userId = resultSet.getLong(COLUMN_NAME_USER_ID);
			}
		} catch (SQLException e) {
			throw new UserDAOException(e + "User with such login: " + login + " doesn't exist. ");
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return userId;
	}

	/**
	 * Create entity User from resultSet
	 * @param rs
	 * @return entity User
	 * @throws SQLException
     */
	private User createUser (ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getLong(COLUMN_NAME_USER_ID));
		user.setName(rs.getString(COLUMN_NAME_USER_NAME));
		user.setLogin(rs.getString(COLUMN_NAME_USER_LOGIN));
		user.setPassword(rs.getString(COLUMN_NAME_USER_PASSWORD));
		return user;
	}

}
