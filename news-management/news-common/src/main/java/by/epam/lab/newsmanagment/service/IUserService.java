package by.epam.lab.newsmanagment.service;

import by.epam.lab.newsmanagment.domain.dto.UserTO;
import by.epam.lab.newsmanagment.exception.service.IServiceException;

/**
 * @author Nastya Dubovik
 */
public interface IUserService {

	/**
	 * Registration user in the database.
	 * Validate parameters.
	 * @param userTO
	 * @return UserTO
	 * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
	 */
	UserTO registration(UserTO userTO) throws IServiceException;

	/**
	 * Login in the system.
	 * @param login
	 * @param password
	 * @return UserTO
	 * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
	 */
	UserTO login(String login, String password) throws IServiceException;

	/**
	 * Delete the user from the database.
	 * @param userTO
	 * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
	 */
	void delete(UserTO userTO) throws IServiceException;

	/**
	 * Edit an information about user.
	 * Validate parameters.
	 * @param userTO
	 * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
	 */
	void edit(UserTO userTO) throws IServiceException;
}
