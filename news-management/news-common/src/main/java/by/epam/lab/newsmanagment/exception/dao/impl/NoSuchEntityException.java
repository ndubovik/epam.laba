package by.epam.lab.newsmanagment.exception.dao.impl;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;

/**
 * Created by Настенька on 3/14/2016.
 */
public class NoSuchEntityException extends IDAOException {
    public NoSuchEntityException(){}
    public NoSuchEntityException(String message, Throwable exception) {
        super(message, exception);
    }
    public NoSuchEntityException(String message) {
        super(message);
    }
    public NoSuchEntityException(Throwable exception) {
        super(exception);
    }
}
