package by.epam.lab.newsmanagment.exception.dao.impl;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception in RoleDAO
 */
public class RoleDAOException extends IDAOException {
    public RoleDAOException(){}
    public RoleDAOException(String message, Throwable exception) {
        super(message, exception);
    }
    public RoleDAOException(String message) {
        super(message);
    }
    public RoleDAOException(Throwable exception) {
        super(exception);
    }
}
