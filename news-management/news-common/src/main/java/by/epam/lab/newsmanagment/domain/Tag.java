package by.epam.lab.newsmanagment.domain;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Nastya Dubovik
 */
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    @Size(min=1, max=30)
    private String name;

    public Tag() {
        this.id = null;
        this.name = null;
    }

    public Tag(String name) {
        this.id = null;
        this.name = name;
    }

    public Tag(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Compare this object with object that are sent
     * @param obj
     * @return true if this object equals to obj, false if this object doesn't equal to obj
     */
    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Tag o = (Tag)obj;
        if(! this.name.equals(o.name)){
            return false;
        }
        return true;
    }

    /**
     * Create a string that contains all information about the object
     * @return string with full information
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": id = " + id);
        sb.append(", name = " + name);
        return sb.toString();
    }

    /**
     * Depending on main fields creat a hash code
     * @return hash code
     */
    @Override
    public int hashCode(){
        int hash = name.hashCode()*31 ;
        return hash;
    }
}
