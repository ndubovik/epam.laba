package by.epam.lab.newsmanagment.domain;


import java.io.Serializable;

/**
 * @author Nastya Dubovik
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long roleId;
    private Long userId;
    private RoleName roleName;

    public Role() {
        this.roleId = null;
        this.userId = null;
        this.roleName = null;
    }

    public Role(RoleName roleName) {
        this.roleId = null;
        this.userId = null;
        this.roleName = roleName;
    }

    public Role(Long userId, RoleName roleName) {
        this.roleId = null;
        this.userId = userId;
        this.roleName = roleName;
    }

    public Role(Long roleId, Long userId, RoleName roleName) {
        this.roleId = roleId;
        this.userId = userId;
        this.roleName = roleName;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public RoleName getRoleName() {
        return roleName;
    }

    public void setRoleName(RoleName roleName) {
        this.roleName = roleName;
    }

    /**
     * Compare this object with object that are sent
     * @param obj
     * @return true if this object equals to obj, false if this object doesn't equal to obj
     */
    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        Role o = (Role)obj;
        if(this.roleId != o.roleId){
            return false;
        }
        if(this.userId != o.userId){
            return false;
        }
        if(! this.roleName.equals(o.roleName)){
            return false;
        }
        return true;
    }

    /**
     * Create a string that contains all information about the object
     * @return string with full information
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": roleId = " + roleId);
        sb.append(", userId = " + userId);
        sb.append(", roleName = " + roleName);
        return sb.toString();
    }

    /**
     * Depending on main fields creat a hash code
     * @return hash code
     */
    @Override
    public int hashCode(){
        int hash = roleId.hashCode()*31 + userId.hashCode()*31 + roleName.hashCode()*31 ;
        return hash;
    }
}
