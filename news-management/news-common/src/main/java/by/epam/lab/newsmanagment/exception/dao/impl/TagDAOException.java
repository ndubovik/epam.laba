package by.epam.lab.newsmanagment.exception.dao.impl;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception in TagDAO
 */
public class TagDAOException extends IDAOException {
    public TagDAOException(){}
    public TagDAOException(String message, Throwable exception) {
        super(message, exception);
    }
    public TagDAOException(String message) {
        super(message);
    }
    public TagDAOException(Throwable exception) {
        super(exception);
    }
}
