package by.epam.lab.newsmanagment.dao.impl.oracle;

import by.epam.lab.newsmanagment.dao.IAuthorDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.exception.dao.impl.AuthorDAOException;
import by.epam.lab.newsmanagment.domain.Author;
import by.epam.lab.newsmanagment.utils.MakeStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.IAuthorDAO
 */
@Component
public class AuthorDAO implements IAuthorDAO {

    private static final String SQL_CREATE_AUTHOR = " INSERT INTO AUTHORS (AUTH_NAME) VALUES (?) ";
    private static final String SQL_SELECT_AUTHOR = " SELECT AUTH_ID, AUTH_NAME, AUTH_EXPIRED FROM AUTHORS WHERE AUTH_ID = ? ";
    private static final String SQL_UPDATE_AUTHOR = " UPDATE AUTHORS SET AUTH_NAME = ? , AUTH_EXPIRED = ?  WHERE AUTH_ID = ? ";
    private static final String SQL_DELETE_AUTHOR = " DELETE FROM AUTHORS WHERE AUTH_ID = ? ";
    private static final String SQL_SELECT_ALL_AUTHORS = " SELECT AUTH_ID, AUTH_NAME, AUTH_EXPIRED FROM AUTHORS ORDER BY AUTH_NAME ASC ";
    private static final String SQL_FIND_AUTHOR_ID = " SELECT AUTH_ID, AUTH_NAME, AUTH_EXPIRED FROM AUTHORS WHERE AUTH_NAME = ? ";
    private static final String SQL_FIND_AUTHOR_ID_BY_NEWS_ID = " SELECT NA_AUTHOR_ID FROM NEWS_AUTHORS WHERE NA_NEWS_ID = ? ";

    private static final String COLUMN_NAME_AUTHOR_ID = "AUTH_ID";
    private static final String COLUMN_NAME_AUTHOR_NAME = "AUTH_NAME";
    private static final String COLUMN_NAME_AUTHOR_EXPIRED = "AUTH_EXPIRED";
    private static final String COLUMN_NAME_NA_AUTHOR_ID = "NA_AUTHOR_ID";


    @Autowired
    private DataSource dataSource;

    @Override
    public Long create(Author author) throws IDAOException {

        Long authorId = null;
        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_CREATE_AUTHOR, new String[] {COLUMN_NAME_AUTHOR_ID})) {
            MakeStatement.make(ps, author.getName());
            countRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            authorId = rs.getLong(1);
        } catch (SQLException e) {
            throw new AuthorDAOException(e + "\nCan not create author : " + author);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new AuthorDAOException("Didn't create author.");

        return authorId;
    }


    @Override
    public Author read(Long authorId) throws IDAOException {

        Author author = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_AUTHOR)) {
            MakeStatement.make(ps, authorId);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                author = createAuthor(rs);
            }
        } catch (SQLException e) {
            throw new AuthorDAOException(e + "\nAuthor with id: " + authorId + " doesn't exist.");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return author;
    }


    @Override
    public List<Author> readAll() throws IDAOException {

        List<Author> authorList = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_SELECT_ALL_AUTHORS)) {
            ResultSet rs = ps.executeQuery();
            authorList = new ArrayList<>();
            while( rs.next() ){
                authorList.add(createAuthor(rs));
            }
        } catch (SQLException e) {
            throw new AuthorDAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return authorList;
    }


    @Override
    public void update(Long authorId, Author author) throws IDAOException {

        int countRows = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
            MakeStatement.make(ps, author.getName(), author.getExpired(), authorId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new AuthorDAOException(e + "\nCan not update author with id : " + authorId + " to author : " + author);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new AuthorDAOException("Didn't update author.");
    }


    @Override
    public void delete(Long authorId) throws IDAOException {
        int countRows;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_DELETE_AUTHOR)) {
            MakeStatement.make(ps, authorId);
            countRows = ps.executeUpdate();
        } catch (SQLException e) {
            throw new AuthorDAOException(e + "\nCan not delete author with id : " + authorId);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        /**
         * Because: preparedStatement returns either the row count for
         * SQL Data Manipulation Language (DML) statements or 0 for
         * SQL statements that return nothing
         */
        if(countRows == 0)
            throw new AuthorDAOException("Didn't delete author.");
    }


    @Override
    public Long findId(String authorName) throws IDAOException {

        Long authorId = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_AUTHOR_ID)) {
            MakeStatement.make(ps, authorName);
            ResultSet rs = ps.executeQuery();
            if( rs.next() ){
                authorId = rs.getLong(COLUMN_NAME_AUTHOR_ID);
            }
        } catch (SQLException e) {
            throw new AuthorDAOException(e + "\nAuthor with such name: " + authorName + " doesn't exist. ");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return authorId;
    }


    @Override
    public Long findIdByNewsId(Long newsId) throws IDAOException {

        Long authorId = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SQL_FIND_AUTHOR_ID_BY_NEWS_ID)) {
            MakeStatement.make(ps, newsId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                authorId = rs.getLong(COLUMN_NAME_NA_AUTHOR_ID);
            }
        } catch (SQLException e) {
            throw new AuthorDAOException(e + "\nAuthor for this newsId: " + newsId + " doesn't exist. ");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return authorId;
    }

    /**
     * Create entity Author from ResultSet
     * @param rs
     * @return entity Author
     * @throws SQLException
     */
    private Author createAuthor (ResultSet rs) throws SQLException {
        Long authorId = rs.getLong(COLUMN_NAME_AUTHOR_ID);
        String name = rs.getString(COLUMN_NAME_AUTHOR_NAME);
        Timestamp expired = rs.getTimestamp(COLUMN_NAME_AUTHOR_EXPIRED);
        return new Author(authorId, name, expired);
    }
}
