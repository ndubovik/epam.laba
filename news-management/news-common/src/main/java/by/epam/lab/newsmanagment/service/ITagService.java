package by.epam.lab.newsmanagment.service;

import by.epam.lab.newsmanagment.domain.Tag;
import by.epam.lab.newsmanagment.exception.service.IServiceException;

import java.util.List;

/**
 * @author Nastya Dubovik
 */
public interface ITagService {

    /**
     * Add tag to database.
     * Validate parameters.
     * @param tag
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return id of row that has been inserted
     */
    Long add (Tag tag) throws IServiceException;

    /**
     * Delete tag from database.
     * @param tagId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    void delete (Long tagId) throws IServiceException;

    /**
     * Edit tag in database.
     * @param tag
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    void edit (Tag tag) throws IServiceException;

    /**
     * Get tag from database by tag id.
     * @param tagId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    Tag getById (Long tagId) throws IServiceException;

    /**
     * Get all tags from database.
     * @param
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return list of all tags
     */
    List<Tag> getAll () throws IServiceException;

    /**
     * Get all tags by news id.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    List<Tag> getAllByNewsId(Long newsId) throws IServiceException;

    Tag getByName(String name) throws IServiceException;
}
