package by.epam.lab.newsmanagment.service.impl;

import by.epam.lab.newsmanagment.dao.ICommentDAO;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Comment;
import by.epam.lab.newsmanagment.service.ICommentService;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.service.ICommentService
 */
@Service("commentService")
public class CommentService implements ICommentService {

    @Autowired
    private ICommentDAO commentDAO;

    @Override
    public Long add(Long newsId, Comment comment) throws IServiceException {
        Long commentId = null;
        try {
            comment.setNewsId(newsId);
            commentId = commentDAO.create(comment);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in CommentService", e);
        }
        return commentId;
    }

    @Override
    public void delete(Long comId) throws IServiceException {
        try {
            commentDAO.delete(comId);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in CommentService", e);
        }
    }

    @Override
    public List<Comment> getAllByNewsId(Long newsId) throws IServiceException {
        List<Comment> commentList = null;
        try {
            List<Long> listOfCommentId = commentDAO.findIdByNewsId(newsId);
            commentList = new ArrayList<>();
            Comment comment = null;
            for(Long id : listOfCommentId){
                comment = commentDAO.read(id);
                commentList.add(comment);
            }

        } catch (IDAOException e) {
            throw new IServiceException("Exception in CommentService", e);
        }
        return commentList;
    }
}
