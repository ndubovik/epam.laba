package by.epam.lab.newsmanagment.domain.dto;

import by.epam.lab.newsmanagment.domain.Author;
import by.epam.lab.newsmanagment.domain.Comment;
import by.epam.lab.newsmanagment.domain.News;
import by.epam.lab.newsmanagment.domain.Tag;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


/**
 * @author Nastya Dubovik
 */
public class NewsTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private News news;
    private Author author;
    private List<Tag> tagList;
    private List<Comment> commentList;

    public NewsTO() {
        this.news = new News();
        this.author = new Author();
        this.tagList = new LinkedList<>();
        this.commentList = new LinkedList<>();
    }

    public NewsTO(News news, Author author, List<Tag> tagList, List<Comment> commentList) {
        this.news = news;
        this.author = author;
        this.tagList = tagList;
        this.commentList = commentList;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        NewsTO o = (NewsTO)obj;
        if(! this.news.equals(o.news)){
            return false;
        }
        if(! this.author.equals(o.author)){
            return false;
        }
        for(Tag tag : o.tagList){
            if(! this.tagList.contains(tag)) {
                return false;
            }
        }
        for(Comment comment : o.commentList){
            if(! this.commentList.contains(comment)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": \nnews = " + news);
        sb.append(", \nauthor = " + author);
        sb.append(", \ntags = ");
        for(Tag tag : tagList){
            sb.append(tag + ", ");
        }
        sb.append(", \ncomments = ");
        for(Comment comment : commentList){
            sb.append(comment + ", ");
        }
        return sb.toString();
    }

    @Override
    public int hashCode(){
        int hash=0;
        hash += news.hashCode();
        hash += author.hashCode();
        for(Tag tag : tagList){
            hash+=tag.hashCode();
        }
        for(Comment comment : commentList){
            hash+=comment.hashCode();
        }
        return hash;
    }

}
