package by.epam.lab.newsmanagment.service.impl;

import by.epam.lab.newsmanagment.dao.IAuthorDAO;
import by.epam.lab.newsmanagment.domain.AuthorRecord;
import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Author;
import by.epam.lab.newsmanagment.exception.dao.impl.NoSuchEntityException;
import by.epam.lab.newsmanagment.service.IAuthorService;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.service.IAuthorService
 */
@Service("authorService")
public class AuthorService implements IAuthorService {

    @Autowired
    private IAuthorDAO authorDAO;

    @Override
    public List<Author> getActiveAuthors() throws IServiceException {
        List<Author> activeAuthorList = new ArrayList<>();
        try {
            List<Author> authorList = authorDAO.readAll();
            for(Author author : authorList){
                if(author.getExpired() == null){
                    activeAuthorList.add(author);
                }
            }
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Author Service", e);
        }
        return activeAuthorList;
    }

    @Override
    public Long add(Author author) throws IServiceException {
        Long authorId = null;
        try {
            if( checkAuthor(author) ){
                authorId = authorDAO.create(author);
            } else {
                authorId = author.getId();
            }
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Author Service", e);
        }
        return authorId;
    }

    @Override
    public void makeExpired(Author author) throws IServiceException {
        try {
            author.setExpired();
            authorDAO.update(author.getId(), author);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Author Service", e);
        }
    }

    @Override
    public Author getByNewsId(Long newsId) throws IServiceException {
        Author author = null;
        try {
            Long authorId = authorDAO.findIdByNewsId(newsId);
            author = authorDAO.read(authorId);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Author Service", e);
        }
        return author;
    }

    @Override
    public Author getById(Long authorId) throws IServiceException {
        Author auth = null;
        try {
            auth = authorDAO.read(authorId);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Author Service", e);
        }
        return auth;
    }

    @Override
    public List<Author> getAll() throws IServiceException {
        List<Author> authorList = null;
        try {
            authorList = authorDAO.readAll();
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Author Service", e);
        }
        return authorList;
    }


    @Override
    public void edit(AuthorRecord authorRecord) throws IServiceException {
        Author author = new Author();
        author.setId( authorRecord.getId() );
        author.setName( authorRecord.getName() );

        if(authorRecord.getIsExpired()){
            author.setExpired();
        }

        try {
            authorDAO.update(author.getId(), author);
        } catch (IDAOException e) {
            throw new IServiceException("Exception in Author Service", e);
        }
    }

    /**
     * Check if author is created.
     * If such author doesn't exist, DAO will throw NoSuchEntityException.
     * @param author
     * @return true if he was created, else false
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
    private boolean checkAuthor (Author author) throws IDAOException {
        try {
            authorDAO.findId(author.getName());
        } catch (NoSuchEntityException e) {
            return true;
        }
        return false;
    }

}