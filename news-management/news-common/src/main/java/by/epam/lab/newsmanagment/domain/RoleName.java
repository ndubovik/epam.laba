package by.epam.lab.newsmanagment.domain;

import java.io.Serializable;


/**
 * @author Nastya Dubovik
 */
public enum RoleName {
    ROLE_ADMIN, ROLE_USER
}
