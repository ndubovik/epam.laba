package by.epam.lab.newsmanagment.service;

import by.epam.lab.newsmanagment.domain.Comment;
import by.epam.lab.newsmanagment.exception.service.IServiceException;

import java.util.List;

/**
 * @author Nastya Dubovik
 */
public interface ICommentService {

    /**
     * Add comment to databse.
     * @param newsId
     * @param comment
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return comment id
     */
    Long add (Long newsId, Comment comment) throws IServiceException;

    /**
     * Delete comment from database.
     * @param comId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return
     */
    void delete (Long comId) throws IServiceException;

    /**
     * Get all comments for that news.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return list of comments
     */
    List<Comment> getAllByNewsId(Long newsId) throws IServiceException;
}
