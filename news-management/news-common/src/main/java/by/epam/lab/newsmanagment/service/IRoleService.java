package by.epam.lab.newsmanagment.service;

import by.epam.lab.newsmanagment.domain.Role;
import by.epam.lab.newsmanagment.exception.service.IServiceException;

/**
 * @author Nastya Dubovik
 */
public interface IRoleService {

    /**
     * Add role to database
     * @param role
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return id of row that has been added
     */
    Long add(Role role) throws IServiceException;

    /**
     * Edit role.
     * @param roleId, role
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    void edit(Long roleId, Role role) throws IServiceException;

    /**
     * Delete role.
     * @param role
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     */
    void delete(Role role) throws IServiceException;

    /**
     * Get role by it's id.
     * @param roleId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return required role
     */
    Role get(Long roleId) throws IServiceException;

    /**
     * Find id of role by user id.
     * @param userId
     * @throws by.epam.lab.newsmanagment.exception.service.IServiceException
     * @return role id of required user id
     */
    Long findIdByUserId(Long userId) throws IServiceException;
}
