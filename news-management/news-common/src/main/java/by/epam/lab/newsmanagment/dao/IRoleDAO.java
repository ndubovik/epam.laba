package by.epam.lab.newsmanagment.dao;


import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.Role;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.GenericDAO
 */
public interface IRoleDAO extends GenericDAO<Long, Role> {

    /**
     * Find role id by user id.
     * @param userId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return role id
     */
    Long findIdByUserId(Long userId) throws IDAOException;
}
