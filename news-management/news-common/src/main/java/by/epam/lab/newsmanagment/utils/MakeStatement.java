package by.epam.lab.newsmanagment.utils;

import by.epam.lab.newsmanagment.domain.RoleName;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 * @author Nastya Dubovik
 */
public class MakeStatement {

    private static final String DATE_TIME_PATTERN = "dd-MMM-YY hh.mm.ss a";
    private static final String DATE_PATTERN = "dd-MMM-YY";

    /**
     * Add to preparedStatement necessary arguments from incoming array
     * @param ps
     * @param array
     * @throws SQLException
     */
    public static void make(PreparedStatement ps, Object... array) throws SQLException {
        for(int i=0; i< array.length ; i++){
            if(array[i] instanceof String){
                ps.setString(i+1, (String)(array[i]));
            }
            if(array[i] instanceof Long){
                ps.setLong(i+1, (Long)(array[i]));
                continue;
            }
            if(array[i] instanceof Integer){
                ps.setInt(i+1, (Integer)(array[i]));
                continue;
            }
            if(array[i] instanceof Timestamp){
                ps.setTimestamp(i+1, (Timestamp)(array[i]));
            }
            if(array[i] instanceof RoleName){
                ps.setString(i+1, array[i].toString());
            }
            if(array[i] == null){
                ps.setString(i+1, null);
            }
            if(array[i] instanceof LocalDateTime){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
                LocalDateTime dateTime = (LocalDateTime) array[i];
                ps.setString(i+1, dateTime.format(formatter));
            }
            if(array[i] instanceof LocalDate){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
                LocalDate date = (LocalDate) array[i];
                ps.setString(i+1, date.format(formatter));
            }
        }
    }

}
