package by.epam.lab.newsmanagment.dao;

import by.epam.lab.newsmanagment.exception.dao.IDAOException;
import by.epam.lab.newsmanagment.domain.News;

import java.util.List;
import java.util.Set;


/**
 * @author Nastya Dubovik
 * @see by.epam.lab.newsmanagment.dao.GenericDAO
 */
public interface INewsDAO extends GenericDAO<Long, News> {

    /**
     * Read all news id sorted by authors and tags.
     * @param authorIdSet
     * @param tagIdSet
     * @return set of news id
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
    List<Long> readAll(Set<Long> tagIdSet, Set<Long> authorIdSet, int newsPerPage, int page) throws IDAOException;

    /**
     * Connect news and author.
     * @param newsId
     * @param authorId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return
     */
    void connectWithAuthor (Long newsId, Long authorId) throws IDAOException;

    /**
     * Connect news and tag.
     * @param newsId
     * @param tagId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return
     */
    void connectWithTag (Long newsId, Long tagId) throws IDAOException;

    /**
     * Disconnect news and author.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return
     */
    void disconnectWithAuthor (Long newsId) throws IDAOException;

    /**
     * Disconnect news and tag.
     * @param newsId
     * @param tagId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return
     */
    void disconnectWithTag (Long newsId, Long tagId) throws IDAOException;

    /**
     * Disconnect news with all tags.
     * @param newsId
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     * @return
     */
    void disconnectWithTags (Long newsId) throws IDAOException;

    /**
     * At first count news that are meet with search criteria. And divide it in pages.
     * @param tagIdSet
     * @param authorIdSet
     * @return amount of pages
     * @throws by.epam.lab.newsmanagment.exception.dao.IDAOException
     */
    int countNews (Set<Long> tagIdSet, Set<Long> authorIdSet) throws IDAOException;
}
