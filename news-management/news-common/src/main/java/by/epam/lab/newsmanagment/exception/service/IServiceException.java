package by.epam.lab.newsmanagment.exception.service;

/**
 * @author Nastya Dubovik
 * Describes exception in service
 */
public class IServiceException extends Exception {
	public IServiceException(){}
    public IServiceException(String message, Throwable exception) {
        super(message, exception);
    }
    public IServiceException(String message) {
        super(message);
    }
    public IServiceException(Throwable exception) {
        super(exception);
    }
}
