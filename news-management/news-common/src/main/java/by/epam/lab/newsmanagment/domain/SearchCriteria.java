package by.epam.lab.newsmanagment.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * @author Nastya Dubovik
 */
public class SearchCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private Set<Long> authorIdSet;
    private Set<Long> tagIdSet;

    public SearchCriteria() {
        this.authorIdSet = new LinkedHashSet<>();
        this.tagIdSet = new LinkedHashSet<>();
    }

    public SearchCriteria(Set<Long> authorIdSet, Set<Long> tagIdSet) {
        this.authorIdSet = authorIdSet;
        this.tagIdSet = tagIdSet;
    }

    public Set<Long> getAuthorIdSet() {
        return authorIdSet;
    }

    public void setAuthorIdSet(Set<Long> authorIdSet) {
        this.authorIdSet = authorIdSet;
    }

    public Set<Long> getTagIdSet() {
        return tagIdSet;
    }

    public void setTagIdSet(Set<Long> tagIdSet) {
        this.tagIdSet = tagIdSet;
    }

    public void setTagId (Long tagId) {
        this.tagIdSet.add(tagId);
    }

    public void setAuthorId (Long authorId) {
        this.authorIdSet.add(authorId);
    }

    public String authorIdSetToQueryString(){
        String str = null;
        if(authorIdSet != null && !authorIdSet.isEmpty()) {
            str = toQueryString(authorIdSet);
        }
        return str;
    }

    public String tagIdSetToQueryString(){
        String str = null;
        if(tagIdSet != null && !tagIdSet.isEmpty()) {
            str = toQueryString(tagIdSet);
        }
        return str;
    }

    private String toQueryString(Set<Long> idSet){
        StringBuffer sb = new StringBuffer();
        for (Long id : idSet) {
            sb.append(id + " ");
        }
        return sb.toString().trim();
    }
}
