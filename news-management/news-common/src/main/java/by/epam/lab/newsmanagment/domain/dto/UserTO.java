package by.epam.lab.newsmanagment.domain.dto;

import by.epam.lab.newsmanagment.domain.Role;
import by.epam.lab.newsmanagment.domain.User;

import java.io.Serializable;


/**
 * @author Nastya Dubovik
 */
public class UserTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private User user;
    private Role role;

    public UserTO() {
        this.user = new User();
        this.role = null;
    }

    public UserTO(User user, Role role) {
        this.user = user;
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        UserTO o = (UserTO)obj;
        if(! this.user.equals(o.user)){
            return false;
        }
        if(! this.role.equals(o.role)){
            return false;
        }
        return true;
    }

    /**
     * Create a string that contains all information about the object
     * @return string with full information
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": \nuser = " + user);
        sb.append(", \nrole = " + role);
        return sb.toString();
    }

    /**
     * Depending on main fields creat a hash code
     * @return hash code
     */
    @Override
    public int hashCode(){
        int hash = user.hashCode()*3 + role.hashCode()*5;
        return hash;
    }
}
