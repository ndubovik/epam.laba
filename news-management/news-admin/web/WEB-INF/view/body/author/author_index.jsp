<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="container body-content">


    <h2>Authors</h2>

    <p>
        <a href="/authors/create">Create New</a>
    </p>
    <table class="table">
        <tr>
            <th>
                Name
            </th>
            <th>
                Status
            </th>

            <th></th>
        </tr>

        <c:forEach var="author" items="${authors}">
            <tr>
                <td>
                        ${author.name}
                </td>
                <td>
                    <c:if test="${author.expired != null}">
                        Expired from ${author.expired}
                    </c:if>
                    <c:if test="${author.expired == null}">
                        Active
                    </c:if>
                </td>

                <td>
                    <a href="/authors/edit/${author.id}">Edit</a>
                </td>
            </tr>
        </c:forEach>


    </table>

    <hr />
    <%--footer--%>
</div>