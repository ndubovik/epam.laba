<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="container body-content">


    <h2>Create</h2>

    <c:url var="create" value="/news/create">
        <c:param name="tags" value="${param.tags}"/>
        <c:param name="authors" value="${param.authors}"/>
    </c:url>

    <sf:form method="POST" action="/news/create" modelAttribute="newsRecord">
        <div class="form-horizontal">
            <h4>News</h4>
            <hr />
            <div class="form-group">
                <label class="control-label col-md-2" for="Title">Title</label>
                <div class="col-md-10">
                    <sf:input class="form-control text-box single-line" id="title" path="news.title" type="text" maxlength="30" />
                    <span class="field-validation-valid text-danger" data-valmsg-for="title" data-valmsg-replace="true"></span>
                    <sf:errors path="news.title" cssStyle="color: #f39c12"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="shortText">Short description</label>
                <div class="col-md-10">
                    <sf:textarea class="form-control text-box single-line" id="shortText" path="news.shortText" type="text" rows="3" maxlength="100" ></sf:textarea>
                    <span class="field-validation-valid text-danger" data-valmsg-for="shortText" data-valmsg-replace="true"></span>
                    <sf:errors path="news.shortText" cssStyle="color: #f39c12"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="fullText">Full text</label>
                <div class="col-md-10">
                    <sf:textarea class="form-control text-box single-line" id="fullText" path="news.fullText" type="text" rows="5" maxlength="2000" ></sf:textarea>
                    <span class="field-validation-valid text-danger" data-valmsg-for="fullText" data-valmsg-replace="true"></span>
                    <sf:errors path="news.fullText" cssStyle="color: #f39c12"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="creationDate">Creation Date</label>
                <div class="col-md-10">
                    <sf:input class="form-control text-box single-line" id="creationDate" type="datetime-local" path="creationDate" value="" max=""/>
                    <span class="field-validation-valid text-danger" data-valmsg-for="creationDate" data-valmsg-replace="true"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2" for="authorId">Author</label>
                <div class="col-md-10">
                    <sf:select class="form-control text-box single-line" id="author" path="authorId">
                        <c:forEach var="auth" items="${authorList}">
                            <sf:option value="${auth.id}">${auth.name}</sf:option>
                        </c:forEach>
                    </sf:select>
                    <sf:errors path="authorId" cssStyle="color: #f39c12"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2">Tags</label>
                <div class="col-md-10">
                    <c:forEach var="tag" items="${tagList}">
                        <sf:checkbox path="tagIdList" value="${tag.id}"/> ${tag.name}
                    </c:forEach>
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <input type="submit" value="Create" class="btn btn-default" />
                </div>
            </div>
        </div>
    </sf:form>
    <div>
        <c:url var="back" value="/news/${param.curPage}">
            <c:param name="authors" value="${param.authors}"/>
            <c:param name="tags" value="${param.tags}"/>
        </c:url>
        <a href="${back}">Back to List</a>
    </div>

    <script src="/resources/scripts/jquery-1.10.2.min.js"></script>
    <script src="/resources/scripts/jquery.validate.min.js"></script>
    <script src="/resources/scripts/jquery.validate.unobtrusive.min.js"></script>
    <script src="/resources/scripts/dateLoader.js"></script>
    <%--footer--%>
</div>
