<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="container body-content">


    <h2>Delete</h2>

    <h3>Are you sure you want to delete this?</h3>
    <div>
        <h4>News</h4>
        <hr />
        <dl class="dl-horizontal">
            <dt>
                Title
            </dt>

            <dd>
                ${newsTO.news.title}
            </dd>

            <br/>

            <dt>
                Context
            </dt>

            <dd>
                ${newsTO.news.fullText}
            </dd>

            <br/>

            <dt>
                Creation Date
            </dt>

            <dd>
                ${newsTO.news.creationDate}
            </dd>

            <br/>

            <dt>
                Modification Date
            </dt>

            <dd>
                ${newsTO.news.modificationDate}
            </dd>

            <br/>

            <dt>
                Author
            </dt>

            <dd>
                ${newsTO.author.name}
            </dd>

            <br/>

            <dt>
                Tags
            </dt>

            <dd>
                <c:forEach var="tag" items="${newsTO.tagList}">
                    ${tag.name}
                </c:forEach>
            </dd>

            <br/>

            <dt>
                Comments
            </dt>

            <dd>
                <c:forEach var="com" items="${newsTO.commentList}">
                    <div>
                            ${com.comText}
                            ${com.comCreationDate}
                        <hr>
                    </div>
                </c:forEach>
            </dd>

        </dl>

        <form action="/news/delete/${newsTO.news.id}" method="post">
            <div class="form-actions no-color">
                <input type="submit" value="Delete" class="btn btn-default" /> |
                <c:url var="back" value="/news/${param.curPage}">
                    <c:param name="authors" value="${param.authors}"/>
                    <c:param name="tags" value="${param.tags}"/>
                </c:url>
                <a href="${back}">Back to List</a>
            </div>
        </form></div>
    <%--footer--%>
</div>