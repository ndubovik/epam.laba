<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<form action="/news" method="get" modelAttribute="searchCriteria" class="col-sm-4" style="position:relative; margin-left: 0;">

    <div class="form-group">
        <label class="control-label col-md-2">Authors</label>
        <div class="col-md-10">
            <c:forEach var="auth" items="${authorList}">
                <sf:checkbox path="searchCriteria.authorIdSet" value="${auth.id}"/> ${auth.name} <br>
            </c:forEach>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label class="control-label col-md-2">Tags</label>
        <div class="col-md-10">
            <c:forEach var="tag" items="${tagList}">
                <sf:checkbox path="searchCriteria.tagIdSet" value="${tag.id}"/> ${tag.name} <br>
            </c:forEach>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <input type="submit" value="Search" class="btn btn-default" />
        </div>
    </div>

    <div class="col-md-offset-2 col-md-10">
        <a href="/news/1" >Reset</a>
    </div>

</form>


<p>

    <c:url var="create" value="/news/create">
        <c:param name="authors" value="${searchCriteria.authorIdSetToQueryString()}"/>
        <c:param name="tags" value="${searchCriteria.tagIdSetToQueryString()}"/>
        <c:param name="curPage" value="${curPage}"/>
    </c:url>

    <security:authorize access="hasRole('ROLE_ADMIN')">
        <a href="${create}">Create New</a>
    </security:authorize>
</p>
<table class="table">
    <tr>
        <th>
            Title
        </th>
        <th>
            Short Description
        </th>
        <th>
            Modification Date
        </th>
        <th>
            Tags
        </th>
        <th>
            Comments
        </th>
        <th></th>
    </tr>

    <c:forEach var="newsTO" items="${newsSet}">
        <tr>
            <td>
                    ${newsTO.news.title}
            </td>
            <td>
                    ${newsTO.news.shortText}
            </td>
            <td>
                    ${newsTO.news.modificationDate}
            </td>
            <td>
                <c:forEach var="tag" items="${newsTO.tagList}">
                    ${tag.name}
                </c:forEach>
            </td>
            <td>
                    ${newsTO.commentList.size()}
            </td>
            <td>
                <c:url var="edit" value="/news/edit/${newsTO.news.id}">
                    <c:param name="authors" value="${searchCriteria.authorIdSetToQueryString()}"/>
                    <c:param name="tags" value="${searchCriteria.tagIdSetToQueryString()}"/>
                    <c:param name="curPage" value="${curPage}"/>
                </c:url>
                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <a href="${edit}">Edit</a> |
                </security:authorize>

                <c:url var="details" value="/news/details/${newsTO.news.id}">
                    <c:param name="authors" value="${searchCriteria.authorIdSetToQueryString()}"/>
                    <c:param name="tags" value="${searchCriteria.tagIdSetToQueryString()}"/>
                    <c:param name="curPage" value="${curPage}"/>
                </c:url>
                <a href="${details}">Details</a>

                <c:url var="delete" value="/news/delete/${newsTO.news.id}">
                    <c:param name="authors" value="${searchCriteria.authorIdSetToQueryString()}"/>
                    <c:param name="tags" value="${searchCriteria.tagIdSetToQueryString()}"/>
                    <c:param name="curPage" value="${curPage}"/>
                </c:url>
                <security:authorize access="hasRole('ROLE_ADMIN')">
                    | <a href="${delete}">Delete</a>
                </security:authorize>
            </td>
        </tr>
    </c:forEach>

</table>

<div>

    <c:url var="previousPage" value="/news/${curPage - 1}">
        <c:param name="authors" value="${searchCriteria.authorIdSetToQueryString()}"/>
        <c:param name="tags" value="${searchCriteria.tagIdSetToQueryString()}"/>
    </c:url>

    <c:if test="${curPage != 1}">
        <a href="${previousPage}"><- Back</a>
    </c:if>

<c:forEach var="i" begin="1" end="${pages}">

    <c:url var="next" value="/news/${i}">
        <c:param name="authors" value="${searchCriteria.authorIdSetToQueryString()}"/>
        <c:param name="tags" value="${searchCriteria.tagIdSetToQueryString()}"/>
    </c:url>

    <a href="${next}">

        <c:if test="${i == curPage}">
            <b> ${i} </b>
        </c:if>

        <c:if test="${i != curPage}">
            ${i}
        </c:if>

    </a> |
</c:forEach>

    <c:url var="nextPage" value="/news/${curPage + 1}">
        <c:param name="authors" value="${searchCriteria.authorIdSetToQueryString()}"/>
        <c:param name="tags" value="${searchCriteria.tagIdSetToQueryString()}"/>
    </c:url>

    <c:if test="${pages > curPage}">
        <a href="${nextPage}">Next -></a>
    </c:if>

</div>