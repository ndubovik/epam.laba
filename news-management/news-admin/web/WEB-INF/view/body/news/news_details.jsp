<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div class="container body-content">


    <h2>Details</h2>

    <div>
        <h4>News</h4>
        <hr />
        <dl class="dl-horizontal">
            <dt>
                Title
            </dt>

            <dd>
                ${newsTO.news.title}
            </dd>

            <br/>

            <dt>
                Full description
            </dt>

            <dd>
                ${newsTO.news.fullText}
            </dd>

            <br/>

            <dt>
                Creation Date
            </dt>

            <dd>
                ${newsTO.news.creationDate}
            </dd>

            <br/>

            <dt>
                Modification Date
            </dt>

            <dd>
                ${newsTO.news.modificationDate}
            </dd>

            <br/>

            <dt>
                Author
            </dt>

            <dd>
                ${newsTO.author.name}
            </dd>

            <br/>

            <dt>
                Tags
            </dt>

            <dd>
                <c:forEach var="tag" items="${newsTO.tagList}">
                    ${tag.name}
                </c:forEach>
            </dd>

            <br/>

            <c:if test="${newsTO.commentList.size() > 0}">
                <dt>
                    Comments
                </dt>

                <dd>
                    <c:forEach var="com" items="${newsTO.commentList}">
                        <div>
                            Time: ${com.comCreationDate}  <br/>
                                ${com.comText}
                            <br/>
                            <security:authorize access="hasRole('ROLE_ADMIN')">
                            <sf:form method="post" action="/news/details/${newsTO.news.id}/deleteCom/${com.comId}">

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <input type="submit" value="Delete" class="btn btn-default" />
                                    </div>
                                </div>

                            </sf:form>
                            </security:authorize>
                            <hr>
                        </div>
                    </c:forEach>

                </dd>
            </c:if>


            <dt>
                Add Comment
            </dt>

            <dd>
                <sf:form method="post" action="/news/details/${newsTO.news.id}/addCom" modelAttribute="comment">
                    <div class="form-group">
                        <div class="col-md-10">
                            <sf:textarea class="form-control text-box single-line" type="text" maxlength="100" path="comText" style="max-width: none; display: block; width: 100%;"></sf:textarea>
                            <sf:errors path="comText" cssStyle="color: #f39c12" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input type="submit" value="Add" class="btn btn-default" />
                        </div>
                    </div>
                </sf:form>
            </dd>

        </dl>
    </div>
    <p>
        <c:url var="edit" value="/news/edit/${newsTO.news.id}">
            <c:param name="authors" value="${param.authors}"/>
            <c:param name="tags" value="${param.tags}"/>
            <c:param name="page" value="${param.curPage}"/>
        </c:url>
        <security:authorize access="hasRole('ROLE_ADMIN')">
            <a href="${edit}">Edit</a> |
            </security:authorize>

        <c:url var="back" value="/news/${param.curPage}">
            <c:param name="authors" value="${param.authors}"/>
            <c:param name="tags" value="${param.tags}"/>
        </c:url>
        <a href="${back}">Back to List</a>
    </p>
    <%--footer--%>
</div>