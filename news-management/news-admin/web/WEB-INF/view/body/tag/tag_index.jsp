<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="container body-content">


    <h2>Tags</h2>

    <p>
        <a href="/tags/create">Create New</a>
    </p>
    <table class="table">
        <tr>
            <th>
                Name
            </th>

            <th></th>
        </tr>

        <c:forEach var="tag" items="${tags}">
            <tr>
                <td>
                        ${tag.name}
                </td>

                <td>
                    <a href="/tags/edit/${tag.id}">Edit</a>|
                    <a href="/tags/delete/${tag.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>


    </table>

    <hr />
    <%--footer--%>
</div>
