<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="container body-content">


    <h2>Edit</h2>


    <sf:form action="/tags/edit/${tag.id}" method="post" modelAttribute="tag">
        <div class="form-horizontal">
            <h4>Tag</h4>
            <hr />


            <div class="form-group">
                <label class="control-label col-md-2" for="name">Name</label>
                <div class="col-md-10">
                    <sf:input class="form-control text-box single-line" id="name" path="name" type="text" value="" maxlength="30" />
                    <sf:errors path="name" cssStyle="color: #f39c12"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <input type="submit" value="Save" class="btn btn-default" />
                </div>
            </div>
        </div>
    </sf:form>
    <div>
        <a href="/tags">Back to List</a>
    </div>

    <script src="/resources/scripts/jquery-1.10.2.min.js"></script>
    <script src="/resources/scripts/jquery.validate.min.js"></script>
    <script src="/resources/scripts/jquery.validate.unobtrusive.min.js"></script>

    <hr />
    <%--footer--%>
</div>