<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="container body-content">


    <h2>Delete</h2>

    <h3>Are you sure you want to delete this?</h3>
    <div>
        <h4>News</h4>
        <hr />
        <dl class="dl-horizontal">
            <dt>
                Name
            </dt>

            <dd>
                ${tag.name}
            </dd>
        </dl>

        <form action="/tags/delete/${tag.id}" method="post">
            <div class="form-actions no-color">
                <input type="submit" value="Delete" class="btn btn-default" /> |
                <a href="/tags">Back to List</a>
            </div>
        </form></div>

    <hr />
    <%--footer--%>; 2016 - Nastya Dubovik</p>
    </footer>
</div>
