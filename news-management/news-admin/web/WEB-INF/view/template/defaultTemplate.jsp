<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<head>
    <title><tiles:insertAttribute name="title" ignore="true"/></title>
    <tiles:insertAttribute name="head"/>
</head>
<body>
<div class="row text-center">
    <tiles:insertAttribute name="header"/>
</div>

<div class="row">
    <div class="col-sm-4 text-center">
        <tiles:insertAttribute name="menu"/>
    </div>
    <div class="col-lg-4">
        <tiles:insertAttribute name="body"/>
    </div>
</div>


<div class="row text-center">
    <tiles:insertAttribute name="footer"/>
</div>

<tiles:insertAttribute name="script"/>
</body>
</html>