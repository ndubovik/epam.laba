<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/resources/content/site.css" rel="stylesheet" type="text/css" />
<link href="/resources/content/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/content/style.css" rel="stylesheet" type="text/css" />
<script src="/resources/scripts/modernizr-2.6.2.js"></script>