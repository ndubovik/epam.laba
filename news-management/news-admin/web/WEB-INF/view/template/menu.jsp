<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="hasRole('ROLE_ADMIN')">
<ul>
    <li><a href="/news">News</a></li>
    <li><a href="/authors">Authors</a></li>
    <li><a href="/tags">Tags</a></li>
</ul>
</security:authorize>