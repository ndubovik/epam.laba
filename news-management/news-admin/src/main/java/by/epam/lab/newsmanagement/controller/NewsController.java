package by.epam.lab.newsmanagement.controller;

import by.epam.lab.newsmanagement.utils.EntityConverter;
import by.epam.lab.newsmanagment.domain.*;
import by.epam.lab.newsmanagment.domain.dto.NewsTO;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import by.epam.lab.newsmanagment.service.IAuthorService;
import by.epam.lab.newsmanagment.service.ICommentService;
import by.epam.lab.newsmanagment.service.INewsService;
import by.epam.lab.newsmanagment.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;


@Controller
public class NewsController {

    private static final String PAGES = "pages";
    private static final String CURRENT_PAGE = "curPage";
    private static final String SEARCH_CRITERIA = "searchCriteria";
    private static final String TAG_LIST = "tagList";
    private static final String AUTHOR_LIST = "authorList";
    private static final String NEWS_SET = "newsSet";
    private static final String NEWS_TO = "newsTO";
    private static final String COMMENT = "comment";
    private static final String NEWS_RECORD = "newsRecord";
    private static final String TAGS = "tags";
    private static final String AUTHORS = "authors";

    @Autowired
    INewsService newsService;

    @Autowired
    IAuthorService authorService;

    @Autowired
    ITagService tagService;

    @Autowired
    ICommentService commentService;

    @RequestMapping(value = { "/news", "/news/{page}" }, method = RequestMethod.GET)
    public String getNews(ModelMap model,
                          @PathVariable Optional<Integer> page,
                          @ModelAttribute(SEARCH_CRITERIA) SearchCriteria searchCriteria,
                          @RequestParam(value = TAGS, required = false) String tags,
                          @RequestParam(value = AUTHORS, required = false) String authors) throws IServiceException {

        searchCriteria = formSearchCriteria(searchCriteria, tags, authors);
        List<NewsTO> news = newsService.getNews(searchCriteria, page.orElse(1));
        Integer numberOfPages = newsService.countPages(searchCriteria);
        model.addAttribute(PAGES, numberOfPages);
        model.addAttribute(CURRENT_PAGE, page.orElse(1));
        model.addAttribute(SEARCH_CRITERIA, searchCriteria);
        model.addAttribute(TAG_LIST, tagService.getAll());
        model.addAttribute(AUTHOR_LIST, authorService.getAll());
        model.addAttribute(NEWS_SET, news);
        System.out.println("====== Get News =======");
        return "news_index";
    }

    @RequestMapping(value = "/news/details/{newsId}", method = RequestMethod.GET)
    public String getNewsDetails(@PathVariable Long newsId,
                                 @ModelAttribute("searchCriteria") SearchCriteria searchCriteria,
                                 ModelMap model) throws IServiceException {
        NewsTO newsTO  = newsService.get(newsId);
        model.addAttribute(SEARCH_CRITERIA, searchCriteria);
        model.addAttribute(NEWS_TO, newsTO);
        model.addAttribute(COMMENT, new Comment());
        System.out.println("====== News Details =======");
        return "news_details";
    }

    @RequestMapping(value = "/news/create", method = RequestMethod.GET)
    public String getCreatePage(ModelMap model) throws IServiceException {
        model.addAttribute(AUTHOR_LIST, authorService.getActiveAuthors());
        model.addAttribute(TAG_LIST, tagService.getAll());
        model.addAttribute(NEWS_RECORD, new NewsRecord());
        System.out.println("====== Create News =======");
        return "news_create";
    }

    @RequestMapping(value = "/news/create", method = RequestMethod.POST)
    public String createNews(
            @Valid NewsRecord newsRecord,
            BindingResult bindingResult,
            ModelMap model) throws IServiceException {

        if (bindingResult.hasErrors()) {
            //return "news_create";
            return getCreatePage(model);
        }

        String creationDate = newsRecord.getCreationDate();
        newsRecord.getNews().setCreationDate(creationDate);
        newsRecord.getNews().setModificationDate(creationDate);

        System.out.println(newsRecord);
        newsService.add(newsRecord);
        System.out.println("====== Create News =======");
        return "redirect:/news";
    }

    @RequestMapping(value = "/news/delete/{newsId}", method = RequestMethod.GET)
    public String getDeletePage(@PathVariable Long newsId, ModelMap model) throws IServiceException {
        model.addAttribute(NEWS_TO, newsService.get(newsId));
        System.out.println("====== Delete News Get =======");
        return "news_delete";
    }

    @RequestMapping(value = "/news/delete/{newsId}", method = RequestMethod.POST)
    public String deleteNews(@PathVariable Long newsId, ModelMap model) throws IServiceException {
        newsService.delete(newsId);
        System.out.println("====== Delete News =======");
        return "redirect:/news";
    }

    @RequestMapping(value = "/news/edit/{newsId}", method = RequestMethod.GET)
    public String getEditPage(@PathVariable Long newsId,
                              ModelMap model) throws IServiceException {
        NewsTO newsTO = newsService.get(newsId);
        NewsRecord newsRecord = EntityConverter.convertToNewsRecord(newsTO);
        model.addAttribute(NEWS_RECORD, newsRecord );
        List<Author> authorList = authorService.getActiveAuthors();
        Author currentAuthor = authorService.getByNewsId(newsId);
        if( ! authorList.contains(currentAuthor) ){
            authorList.add(currentAuthor);
        }
        model.addAttribute(AUTHOR_LIST, authorList );
        model.addAttribute(TAG_LIST, tagService.getAll() );
        System.out.println("====== Edit News Get =======");
        return "news_edit";
    }

    @RequestMapping(value = "/news/edit/{newsId}", method = RequestMethod.POST)
    public String editNews(
            @PathVariable Long newsId,
            @Valid NewsRecord newsRecord,
            BindingResult bindingResult,
            ModelMap model ) throws IServiceException {

        if (bindingResult.hasErrors()) {
            //return "news_edit";
            return getEditPage(newsId, model);
        }
        LocalDate modificationDate = new Timestamp(System.currentTimeMillis()).toLocalDateTime().toLocalDate();
        newsRecord.getNews().setModificationDate(modificationDate);
        newsRecord.getNews().setId(newsId);
        newsService.edit(newsRecord);
        System.out.println("====== Edit News =======");
        return "redirect:/news";
    }

    @RequestMapping(value = "/news/details/{newsId}/addCom", method = RequestMethod.POST)
    public String addComment(
            @PathVariable Long newsId,
            @Valid Comment comment,
            BindingResult bindingResult,
            @ModelAttribute(SEARCH_CRITERIA) SearchCriteria searchCriteria,
            ModelMap model ) throws IServiceException {

        if (bindingResult.hasErrors()) {
            //return "news_details";
            //return getNewsDetails(newsId, searchCriteria, model);
        }
        Timestamp creationDate = new Timestamp(System.currentTimeMillis());
        comment.setComCreationDate(creationDate);
        comment.setNewsId(newsId);
        commentService.add(newsId, comment);
        System.out.println("====== Add comment =======");
        return "redirect:/news/details/" + newsId;
    }

    @RequestMapping(value = "/news/details/{newsId}/deleteCom/{comId}", method = RequestMethod.POST)
    public String deleteComment(
            @PathVariable Long newsId,
            @PathVariable Long comId,
            ModelMap model ) throws IServiceException {

        commentService.delete(comId);
        System.out.println("====== Delete comment =======");
        return "redirect:/news/details/" + newsId;
    }

    /**
     * Form SearchCriteria from input parameters (query string or modelAttribute)
     * @param searchCriteria
     * @param tags
     * @param authors
     * @return SearchCriteria
     */
    private SearchCriteria formSearchCriteria(SearchCriteria searchCriteria, String tags, String authors){

        if(searchCriteria == null){
            searchCriteria = new SearchCriteria();
        }

        if(searchCriteria.getAuthorIdSet() == null ){
            searchCriteria.setAuthorIdSet(Collections.EMPTY_SET);
        }

        if(searchCriteria.getTagIdSet() == null ){
            searchCriteria.setTagIdSet(Collections.EMPTY_SET);
        }

        if(tags != null){
            searchCriteria.setTagIdSet(parseStringOfId(tags));
        }

        if(authors != null){
            searchCriteria.setAuthorIdSet(parseStringOfId(authors));
        }

        return  searchCriteria;
    }

    /**
     * Parse string to get id set
     * @param str
     * @return id set
     */
    private Set<Long> parseStringOfId(String str){
        Set<Long> idSet;
        if( ! str.isEmpty() ){
            idSet = new HashSet<>();
            String[] stringArray = str.split("\\s");
            for(String author : stringArray){
                idSet.add(Long.parseLong(author));
            }
        } else {
            idSet = Collections.EMPTY_SET;
        }

        return idSet;
    }

}
