package by.epam.lab.newsmanagement.controller;

import by.epam.lab.newsmanagment.exception.service.IServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class LoginController {

    private static final String ERROR = "error";

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login( @RequestParam(value = ERROR, required = false) String error,
                         ModelMap model) throws IServiceException {
        if(error != null){
            model.addAttribute(ERROR, "Invalid username or password");
        }
        System.out.println("==== login =====");
       return "login";
    }

    @RequestMapping(value = "/logout")
    public String logout(){
        return "login";
    }
}
