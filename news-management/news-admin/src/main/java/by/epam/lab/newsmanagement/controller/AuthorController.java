package by.epam.lab.newsmanagement.controller;

import by.epam.lab.newsmanagement.utils.EntityConverter;
import by.epam.lab.newsmanagment.domain.Author;
import by.epam.lab.newsmanagment.domain.AuthorRecord;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import by.epam.lab.newsmanagment.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AuthorController {

    private static final String AUTHORS = "authors";

    @Autowired
    IAuthorService authorService;

    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    public String getAuthors(ModelMap model) throws IServiceException {
        List<Author> authorList = authorService.getAll();
        model.addAttribute(AUTHORS, authorList);
        System.out.println("====== Get Authors =======");
        return "author_index";
    }

    @RequestMapping(value = "/authors/create", method = RequestMethod.GET)
    public String getCreatePage(ModelMap model) throws IServiceException {
        model.addAttribute(AUTHORS , new Author());
        System.out.println("====== Create Authors GET =======");
        return "author_create";
    }

    @RequestMapping(value = "/authors/create", method = RequestMethod.POST)
    public String createAuthor(
            @Valid Author author,
            BindingResult bindingResult,
            ModelMap model) throws IServiceException {

        if (bindingResult.hasErrors()) {
            //return "author_create";
            return getCreatePage(model);
        }

        authorService.add(author);
        System.out.println("====== Create Authors =======");
        return "redirect:/authors";
    }

    @RequestMapping(value = "/authors/edit/{authorId}", method = RequestMethod.GET)
    public String getEditPage(
            @PathVariable Long authorId,
            ModelMap model) throws IServiceException {
        Author author = authorService.getById(authorId);
        AuthorRecord authorRecord = EntityConverter.convertToAuthorRecord(author);
        System.out.println("====== Edit Authors GET =======");
        model.addAttribute(AUTHORS, authorRecord);
        return "author_edit";
    }

    @RequestMapping(value = "/authors/edit/{authorId}", method = RequestMethod.POST)
    public String editAuthor(
            @PathVariable Long authorId,
            @Valid AuthorRecord authorRecord,
            BindingResult bindingResult,
            ModelMap model) throws IServiceException {

        if (bindingResult.hasErrors()) {
            //return "author_edit";
            return getEditPage(authorId, model);
        }

        authorRecord.setId(authorId);
        authorService.edit(authorRecord);
        System.out.println("====== Edit Authors =======");
        return "redirect:/authors";
    }

}
