package by.epam.lab.newsmanagement.controller;

import by.epam.lab.newsmanagment.domain.Tag;
import by.epam.lab.newsmanagment.exception.service.IServiceException;
import by.epam.lab.newsmanagment.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;


@Controller
public class TagController {

    private static final String TAG = "tag";
    private static final String TAGS = "tags";

    @Autowired
    ITagService tagService;

    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public String getTags(ModelMap model) throws IServiceException {
        List<Tag> tagList = tagService.getAll();
        model.addAttribute(TAGS, tagList);
        System.out.println("====== Get Tags =======");
        return "tag_index";
    }

    @RequestMapping(value = "/tags/create", method = RequestMethod.GET)
    public String getCreatePage(ModelMap model) throws IServiceException {
        model.addAttribute(TAG, new Tag());
        System.out.println("====== Create Tag GET =======");
        return "tag_create";
    }

    @RequestMapping(value = "/tags/create", method = RequestMethod.POST)
    public String createTag(
            @Valid Tag tag,
            BindingResult bindingResult,
            ModelMap model) throws IServiceException {

        if (bindingResult.hasErrors()) {
            //return "tag_create";
            return getCreatePage(model);
        }
        tagService.add(tag);
        System.out.println("====== Create Tag =======");
        return "redirect:/tags";
    }

    @RequestMapping(value = "/tags/edit/{tagId}", method = RequestMethod.GET)
    public String getEditPage(
            @PathVariable Long tagId,
            ModelMap model) throws IServiceException {

        Tag tag = tagService.getById(tagId);
        model.addAttribute(TAG, tag);
        return "tag_edit";
    }

    @RequestMapping(value = "/tags/edit/{tagId}", method = RequestMethod.POST)
    public String editTag(
            @PathVariable Long tagId,
            @Valid Tag tag,
            BindingResult bindingResult,
            ModelMap model) throws IServiceException {

        if (bindingResult.hasErrors()) {
            //return "tag_edit";
            return getEditPage(tagId, model);
        }

        tag.setId(tagId);
        tagService.edit(tag);
        System.out.println("====== Edit Tags =======");
        return "redirect:/tags";
    }

    @RequestMapping(value = "/tags/delete/{tagId}", method = RequestMethod.GET)
    public String getDeletePage(@PathVariable Long tagId, ModelMap model) throws IServiceException {
        model.addAttribute(TAG , tagService.getById(tagId) );
        System.out.println("====== Delete Tag GET =======");
        return "tag_delete";
    }

    @RequestMapping(value = "/tags/delete/{tagId}", method = RequestMethod.POST)
    public String deleteTag(@PathVariable Long tagId, ModelMap model) throws IServiceException {
        tagService.delete(tagId);
        System.out.println("====== Delete Tag =======");
        return "redirect:/tags";
    }

}
