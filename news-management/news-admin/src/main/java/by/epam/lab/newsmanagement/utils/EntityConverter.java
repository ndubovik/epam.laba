package by.epam.lab.newsmanagement.utils;

import by.epam.lab.newsmanagment.domain.Author;
import by.epam.lab.newsmanagment.domain.AuthorRecord;
import by.epam.lab.newsmanagment.domain.NewsRecord;
import by.epam.lab.newsmanagment.domain.Tag;
import by.epam.lab.newsmanagment.domain.dto.NewsTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Настенька on 5/1/2016.
 */
public class EntityConverter {

    /**
     * Convert NewsTO to NewsRecord
     * @param newsTO
     * @return newsRecord
     */
    public static NewsRecord convertToNewsRecord (NewsTO newsTO){
        NewsRecord newsRecord = new NewsRecord();
        newsRecord.setNews( newsTO.getNews() );
        newsRecord.setAuthorId( newsTO.getAuthor().getId() );
        List<Tag> tagList = newsTO.getTagList();
        List<Long> tagIdList = new ArrayList<>();
        for( Tag tag : tagList ){
            tagIdList.add( tag.getId() );
        }
        newsRecord.setTagIdList( tagIdList );
        return newsRecord;
    }

    /**
     * Convert Author to AuthorRecord
     * @param author
     * @return authorRecord
     */
    public static AuthorRecord convertToAuthorRecord(Author author){
        AuthorRecord authorRecord = new AuthorRecord();
        authorRecord.setId( author.getId() );
        authorRecord.setName( author.getName() );
        authorRecord.setIsExpired( (author.getExpired() != null) ? true : false );
        return authorRecord;
    }
}
